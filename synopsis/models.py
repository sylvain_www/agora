from django.db import models
from django.contrib.auth.models import User
from agora.models import DeletedContent
from django_markdown.models import MarkdownField
from django.utils.translation import ugettext_lazy as _
from mptt.models import MPTTModel, TreeForeignKey

class Synopsis(MPTTModel):
    parent = TreeForeignKey('self',
        null            = True,
        blank           = True,
        related_name    = 'children')

    content = MarkdownField()

    creator = models.ForeignKey(
        to              = User,
        related_name    = 'redactorOf')

    creation_date = models.DateTimeField(
        auto_now_add    = True)

    deleted = models.OneToOneField(
        to              = DeletedContent,
        null            = True,
        blank           = True)
