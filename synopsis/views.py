from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.http import HttpResponseBadRequest
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
import logging
from proposal.models import Proposal
from .models import Synopsis
from agora.forms import ModifyForm
from .forms import AddForm
from django.core.urlresolvers import reverse
from django.db import transaction
from django.utils.translation import ugettext as _
from agora.models import DeletedContent, DeletedReason
from agora.views import searchform

logger = logging.getLogger(__name__)

@login_required
def view(request, pk, slug):
    searchbox = searchform(request)
    p = get_object_or_404(Proposal, pk=pk)

    # reload object from DB to ensure mptt tree sync
    # TODO use select_related
    s = get_object_or_404(Synopsis, pk=p.synopsis_root.pk)
    synopsia = s.get_descendants(include_self = True)

    popup_title = _("Edition of the synopsis")
    return render(request, 'synopsis/view.html', locals())

@login_required
def select(request, pk, slug, synopsis):
    searchbox = searchform(request)
    p = get_object_or_404(Proposal, pk=pk)
    s = get_object_or_404(Synopsis, pk=synopsis)
    if (s.tree_id != p.synopsis_root.tree_id or
        s.deleted or p.synopsis == s):
        return HttpResponseBadRequest("Bad request")

    if request.method == 'POST':
        if 'select' in request.POST:
            form = ModifyForm(request.POST)
            if not form.is_valid():
                return render(request, 'synopsis/select.html', locals())

            with transaction.atomic():
                p.synopsis = s
                p.save()

        return HttpResponseRedirect(reverse('proposal:synopsis:view',
                                   kwargs = { 'pk': pk, 'slug': slug }))

    form = ModifyForm()
    return render(request, 'synopsis/select.html', locals())

@login_required
def add(request, pk, slug):
    searchbox = searchform(request)
    p = get_object_or_404(Proposal, pk=pk)

    if request.method == 'POST':
        if 'add' in request.POST:
            form = AddForm(request.POST)
            if not form.is_valid():
                return render(request, 'synopsis/add.html', locals())

            with transaction.atomic():
                if p.synopsis.content != request.POST.get('content'):
                    s = Synopsis(content = request.POST.get('content'),
                                 creator = request.user)
                    # todo: log, as proof in case of issue
                    # todo: publish comment as message
                    o = get_object_or_404(Synopsis, pk=p.synopsis.pk
                                    ).get_descendants(include_self=True).last()
                    s.insert_at(o, save=True)
                    p.synopsis = s
                    p.save()

        return HttpResponseRedirect(reverse('proposal:synopsis:view',
                               kwargs = { 'pk': pk, 'slug': slug }))

    synopsis = request.GET.get('pk')
    if synopsis:
        s = get_object_or_404(Synopsis, pk=synopsis)
        if not s.tree_id == p.synopsis_root.tree_id or s.deleted:
            return HttpResponseBadRequest("Bad request")
        c = s.content
    else:
        c = ''

    form = AddForm(initial = { 'content': c })
    return render(request, 'synopsis/add.html', locals())

@login_required
def delete(request, pk, slug, synopsis):
    searchbox = searchform(request)
    p = get_object_or_404(Proposal, pk=pk)
    s = get_object_or_404(Synopsis, pk=synopsis)
    if (s.tree_id != p.synopsis.tree_id or
        s == p.synopsis_root or
        s.deleted):
        return HttpResponseBadRequest("Bad request")

    if request.method == 'POST':
        # Todo: only moderator can delete
        if 'delete' in request.POST:
            form = ModifyForm(request.POST)
            if not form.is_valid():
                return render(request, 'synopsis/delete.html', locals())

            with transaction.atomic():
                if p.synopsis == s:
                    p.synopsis = p.synopsis_root
                    p.save()
                r = DeletedReason.objects.get(label='other') #todo
                s.deleted = DeletedContent.objects.create(
                                reason = r,
                                user   = request.user)
                s.save()
                # todo: publish message

        return HttpResponseRedirect(reverse('proposal:synopsis:view',
                               kwargs = { 'pk': pk, 'slug': slug }))

    form = ModifyForm()
    return render(request, 'synopsis/delete.html', locals())

def markdown(request):
    return render(request, 'synopsis/help.html', locals())

@login_required
def single(request, pk, slug, synopsis):
    searchbox = searchform(request)
    p = get_object_or_404(Proposal, pk=pk)
    s = get_object_or_404(Synopsis, pk=synopsis)
    source = True if request.GET.get('src') else False
    if (not s in p.synopsis_root.get_descendants(include_self=True) or
            s.deleted):
        return HttpResponseBadRequest("Bad request")
    extend = 'agora/flat_popup.html'
    return render(request, 'synopsis/single.html', locals())
