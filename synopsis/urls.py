from django.conf.urls import patterns, include, url

urlpatterns = patterns('synopsis.views',
    url(r'^$', 'view', name='view'),
    url(r'^a/$', 'add', name='add'),
    url(r'^s/(?P<synopsis>\d+)/$', 'select', name='select'),
    url(r'^d/(?P<synopsis>\d+)/$', 'delete', name='delete'),
    url(r'^v/(?P<synopsis>\d+)/$', 'single', name='single'))
