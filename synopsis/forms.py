from django import forms
from agora.forms import ModifyForm
from django_markdown.fields import MarkdownFormField
from django.utils.translation import ugettext_lazy as _
import logging

logger = logging.getLogger(__name__)

class AddForm(ModifyForm):
    content = MarkdownFormField()
