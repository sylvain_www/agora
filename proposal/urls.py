from django.conf.urls import patterns, include, url

urlpatterns_slug = patterns('proposal.views',
    url(r'^$', 'content', name='content'),
    url(r'^d/$', 'discuss', name='discuss'),
    url(r'^r/$', 'reference', name='reference'),
    url(r'^s/$', 'statistics', name='statistics'),
    url(r'^c/$', 'charter', name='charter'),
    url(r'^h/', include('synopsis.urls', namespace='synopsis')),
    url(r'^t/', include('tag.urls_proposal', namespace='tag')),
    url(r'^i/', include('cover.urls', namespace='cover')))

urlpatterns = patterns('proposal.views',
    url(r'^(?P<slug>[-\w]+)/', include(urlpatterns_slug)),
    url(r'^$', 'shortcut', name='shortcut'))
