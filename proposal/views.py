from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.http import HttpResponseBadRequest
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from proposal.models import Proposal, Relation, Link
from django.contrib.auth.models import User
from control.views import get_control_form, apply_control_form
from place.utils import get_breadcrumbs
from django.core.urlresolvers import reverse
import operator
from django.db.models import Q
from django.utils.translation import ungettext
from functools import reduce
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import  place.views
import agora.views
import logging

logger = logging.getLogger(__name__)

def shortcut(request, pk):
    p = get_object_or_404(Proposal, pk=pk)
    return HttpResponseRedirect(reverse('proposal:content',
                                       kwargs = { 'pk': pk, 'slug': p.slug }))

def common(request, pk, slug):
    c = {}
    if request.method == 'POST':
        if not request.user.is_authenticated():
           return (c, redirect("%s?next=%s" % (reverse('account:login'), request.path)))
        if not apply_control_form(request):
            return (c, HttpResponseBadRequest("Bad request"))

    p = get_object_or_404(Proposal, pk=pk)
    if slug != p.slug:
       return (c, HttpResponseRedirect(reverse('proposal:content',
                                       kwargs = { 'pk': pk, 'slug': p.slug })))

    c['p'] = p
    c['control_form'] = get_control_form(p, request.user)
    c['control_callback'] = request.path_info
    c['breadcrumbs'] = get_breadcrumbs(p.place)
    c['searchbox'] = agora.views.searchform(request)
    return (c, None)

def discuss(request, pk, slug):
    (context, error) = common(request, pk, slug)
    if error:
        return error

    return render(request, 'proposal/discuss.html', context)

def statistics(request, pk, slug):
    (context, error) = common(request, pk, slug)
    if error:
        return error

    return render(request, 'proposal/statistics.html', context)

def reference(request, pk, slug):
    (context, error) = common(request, pk, slug)
    if error:
        return error

    return render(request, 'proposal/reference.html', context)

def charter(request, pk, slug):
    (context, error) = common(request, pk, slug)
    if error:
        return error

    return render(request, 'proposal/charter.html', context)

def content(request, pk, slug):
    (context, error) = common(request, pk, slug)
    if error:
        return error

    counters = []
    p = context['p']
    c = p.infavor.count()
    counters.append({ 'label': ungettext('in favor', 'in favor', c), 'count': c, 'url': None})
    c = p.against.count()
    counters.append({ 'label': ungettext('against', 'against', c), 'count': c, 'url': None })
    c = p.undecided.count()
    counters.append({ 'label': ungettext('undecided', 'undecided', c), 'count': c, 'url': None })
    c = p.followers.count()
    counters.append({ 'label': ungettext('follower', 'followers', c), 'count': c, 'url': None })
    c = p.visitors
    counters.append({ 'label': ungettext('visitor', 'visitors', c), 'count': c, 'url': None })

    for r in Relation.objects.all():
        label = r.label
        labels = r.label_plural

        # bidirectional relations
        q = Link.objects.filter(Q(proposal = p) &
                                Q(relation = r) &
                                ~Q(reverse_link = None))
        c = q.count()
        if c:
            if c == 1:
                o = q.first().other
                url = reverse('proposal:content',
                                    kwargs = {'pk': o.pk, 'slug': o.slug })
            else:
                url = "%s?ps=%s&pr=%s&pb=0" % (reverse('listing:standard'),
                                                p.pk, r.pk)
            counters.append({ 'label': ungettext(label, labels, c) + " (bidir)",
                              'count': c,
                              'url': url })

        # outgoing relations (pb=1)
        q = Link.objects.filter(proposal = p,
                                relation = r,
                                reverse_link = None)
        c = q.count()
        if c:
            if c == 1:
                o = q.first().other
                url = reverse('proposal:content',
                                    kwargs = {'pk': o.pk, 'slug': o.slug })
            else:
                url = "%s?ps=%s&pr=%s&pb=1" % (reverse('listing:standard'),
                                                p.pk, r.pk)
            counters.append({ 'label': ungettext(label, labels, c) + " (out)",
                              'count': c,
                              'url': url })

        # incoming relations (pb=1)
        q = Link.objects.filter(other = p,
                                relation = r.reverse_relation,
                                reverse_link = None)
        c = q.count()
        if c:
            if c == 1:
                o = q.first().proposal
                url = reverse('proposal:content',
                                    kwargs = {'pk': o.pk, 'slug': o.slug })
            else:
                url = "%s?pS=%s&pR=%s&pB=1" % (reverse('listing:standard'),
                                                p.pk, r.reverse_relation.pk)
            counters.append({ 'label': ungettext(label, labels, c) + " (in)",
                              'count': c,
                              'url': url })

    context['counters'] = counters
    return render(request, 'synopsis/content.html', context)
