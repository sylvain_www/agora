from django.utils.translation import ungettext

# hardcoded strings for maketranslation discovery
# do not include module

# matches fixture in relations.json
ungettext('parent', 'parents', 0)
ungettext('child', 'children', 0)
ungettext('related', 'related', 0)
ungettext('opposed', 'opposed', 0)
ungettext('programme', 'programmes', 0)
ungettext('item', 'items', 0)
ungettext('origin', 'origins', 0)
ungettext('clone', 'clones', 0)
