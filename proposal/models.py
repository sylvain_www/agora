from django.contrib.auth.models import User
from django.utils import timezone
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.template.defaultfilters import slugify
from unidecode import unidecode
from place.models import GenericPlace
from synopsis.models import Synopsis
from tag.models import Tag
import logging
from cover.models import Cover
#import pdb; pdb.set_trace()

logger = logging.getLogger(__name__)

class Proposal(models.Model):
    title = models.CharField(
        verbose_name    = _('title'),
        max_length      = 140,
        unique          = False,
        blank           = False,
        null            = False,
        help_text       = _('title'))

    synopsis_root = models.OneToOneField(
        to              = Synopsis,
        related_name    = 'synopsis_root_of')

    synopsis = models.OneToOneField(
        to              = Synopsis,
        related_name    = 'synopsis_of')

    creator = models.ForeignKey(
        to              = User,
        related_name    = 'creatorOf',
        editable        = False) # hide from admin and form

    #verifiers = models.ManyToManyField(
    # SHOULD BE IN LOG ONLY, WITH DATE AND USER

    cover_root = models.OneToOneField(
        to              = Cover,
        related_name    = 'cover_root_of')

    cover = models.OneToOneField(
        to              = Cover,
        related_name    = 'cover_of')

    place = models.ForeignKey(GenericPlace)

#    affiliate = models.CharField(
#        verbose_name    = _('charity affiliate'),
#        max_length      = 20,
#        unique          = False,
#        help_text       = _('charity affiliate'))

    slug = models.SlugField(
        verbose_name    = _('slug'),
        max_length      = 140,
        blank           = False,
        null            = False,
        editable        = False, # hide from admin and form
        help_text       = _('autogenerated, provides fixed url'))

    creation_date = models.DateTimeField(
        verbose_name    = _('date of creation'),
        editable        = False, # hide from admin and form
        help_text       = _('autogenerated'),
        auto_now_add    = True)

    #moderators = models.ManyToManyField(
    #    to              = User,
    #    related_name    = 'moderatorOf',
    #    editable        = False) # hide from admin and form
    # USE USER GROUPS

    sponsors = models.ManyToManyField(
        to              = User,
        related_name    = 'sponsorOf',
        editable        = False) # hide from admin and form

    infavor = models.ManyToManyField(
        to              = User,
        related_name    = 'infavor',
        editable        = False) # hide from admin and form

    against = models.ManyToManyField(
        to              = User,
        related_name    = 'against',
        editable        = False) # hide from admin and form

    undecided = models.ManyToManyField(
        to              = User,
        related_name    = 'undecided',
        editable        = False) # hide from admin and form

    tags = models.ManyToManyField(
        to              = Tag,
        related_name    = 'tags')

    followers = models.ManyToManyField(
        to              = User,
        related_name    = 'follower',
        editable        = False) # hide from admin and form

    active = models.BooleanField(
        verbose_name    = _('active flag'),
        editable        = False, # hide from admin and form
        default         = False,
        help_text       = _('active flag'))

    restricted = models.BooleanField(
        verbose_name    = _('restricted'),
        default         = True,
        help_text       = _('restricted'))

    recruiting = models.BooleanField(
        verbose_name    = _('recruiting'),
        default         = False,
        help_text       = _('proposal is recruting moderators'))

    visitors = models.IntegerField(
        verbose_name    = _('number of visits'),
        editable        = False, # hide from admin and form
        default         = 0,
        help_text       = _('autopopulated'))

    related = models.ManyToManyField(
        to              = 'self',
        through         = 'Link',
        through_fields  = ('proposal', 'other'),
        symmetrical     = False, # required for a 'through' MtM
        editable        = False) # hide from admin and form

    def validate_title(self, value):
        if value.strip() == '':
            logger.info("trying to enter a proposal with empty title")
            raise ValidationError(_("Title is empty."))

    def save(self, *args, **kwargs):
        if not self.id:
            # newly created object
            self.title = self.title[0:1].capitalize() + self.title[1:]
            self.slug = slugify(unidecode(self.title))[:140]
            U = User.objects.get(pk=1)
            c = Cover.objects.create(
                   creator = U)
            self.cover_root = c
            self.cover = c
            s = Synopsis.objects.create(
                   content = default_synopsis(),
                   comment = _('default content'),
                   creator = U)
            self.synopsis_root = s
            self.synopsis = s

        self.full_clean()
        super(Proposal, self).save(*args, **kwargs)

    class Meta:
        verbose_name = _('proposal')
        verbose_name_plural = _('proposals')

    def __str__(self):
        return self.title

class Relation(models.Model):
    label = models.CharField(
        verbose_name    = (_('status of link')),
        max_length      = 10,
        help_text       = _('status of link'))

    label_plural = models.CharField(
        verbose_name    = (_('status of link')),
        max_length      = 10,
        help_text       = _('status of link'))

    reverse_relation = models.ForeignKey('self')

    def __str__(self):
        return self.label

class Link(models.Model):
    other = models.ForeignKey(Proposal, related_name = 'referencedBy')
    proposal = models.ForeignKey(Proposal)
    date = models.DateTimeField(auto_now_add = True)
    user = models.ForeignKey(User)
    relation = models.ForeignKey(Relation)
    reverse_link = models.ForeignKey('self', null = True)

    def save(self, *args, **kwargs):
        self.full_clean()
        # correctly set reverse
        # todo

        return super(Link, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        # correctly reset reverse
        # todo

        return super(Link, self).delete(*args, **kwargs)

    def __str__(self):
        c = '<->' if self.reverse_link else ' ->'
        return '%(source)s %(connector)s (%(relation)s) %(connector)s %(destination)s' % {
                'source': self.proposal.title,
                'connector': c,
                'relation': self.relation.label,
                'destination': self.other.title}

def default_synopsis():
    return _(
"""# This proposal does not have a detailed description yet.

You are reading the default text associated with each new proposal. Read further to learn more about what to do next!

The title of the proposal is set in stone and cannot change. This text, on the contrary, is here to evolve and mature at the same pace that your proposal is enriched by time and the contributions of other users.

## Edit this text now to provide further information:
* *key figures*,
* elements for *debate*,
* *links* to interesting resource
* etc, ...

## The next steps could be:
* choose who can *participate*
* define a *charter*
* hire *moderators*
* *promote* your idea
* *link* related proposals
* etc, etc, ...

## The weekly life cycle:

Proposals made on SITE NAME follow a weekly life cycle. The week runs from Tuesday 12am to the next Monday at midnight. At the end of the week, the statistics of each proposal are gathered and updated:

* The number of votes in favor, against or undecided, is computed and published on the statistics page
* The number of POKE received during the week is also counted, and the top scoring proposals make it to the leader's score board for that week

The proposal scoring the most during a given week will receive a special promotion the following week! Make sure you gather as many pokes as you can, through advertisement and promotion of your idea.

On the other hand, any proposal that does not receive any POKE during an entire week, will meet a terrible fate and 'die'. The abandonned proposals will turn to read-only mode, and no one, creator included, will be able to modify or comment further. But hope is permitted: anyone, at any time, can clone a proposal, and become the new supporter of an idea."""
)
