from django.test import TestCase
from proposal.models import *

class ModelTestCase(TestCase):
    def test_limits(self):
        p = Proposal()
        # empty title
        p.content = ''
        # slug
        # capitalised title
        # creation date
        # last_update on creation
        # owner = creator
        # last_update is updated when fields are updated
