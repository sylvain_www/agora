from django.shortcuts import render
from control.forms import ControlForm
from django.core.exceptions import ObjectDoesNotExist
from proposal.models import Proposal

def apply_control_form(request):
    d = request.POST
    u = request.user
    f = ControlForm(request.POST)
    if not f.is_valid():
        return False
    try:
        p = Proposal.objects.get(pk=f.cleaned_data['proposal'])
    except ObjectDoesNotExist:
        return False
    if 'infavor' in d:
        if f.cleaned_data['vote'] != 1:
            p.infavor.add(u)
            p.against.remove(u)
            p.undecided.remove(u)
    elif 'against' in d:
        if f.cleaned_data['vote'] != 2:
            p.infavor.remove(u)
            p.against.add(u)
            p.undecided.remove(u)
    elif 'undecided' in d:
        if f.cleaned_data['vote'] != 3:
            p.infavor.remove(u)
            p.against.remove(u)
            p.undecided.add(u)
    elif 'reset' in d:
        if f.cleaned_data['vote'] != 0:
            p.infavor.remove(u)
            p.against.remove(u)
            p.undecided.remove(u)
    elif 'follow' in d:
        if f.cleaned_data['followed'] != 0:
            p.followers.remove(u)
        else:
            p.followers.add(u)
#    elif 'poke' in d:
#        if f.cleaned_data['poked'] != 0:
#            p.poked.remove(u)
#        else:
#            p.poked.add(u)
    return True

def get_control_form(proposal, user):
    if not user or not user.is_authenticated():
        v = 0
        f = 0
        p = 0
    else:
        v = 0
        if user in proposal.infavor.all():
            v = 1
        elif user in proposal.against.all():
            v = 2
        elif user in proposal.undecided.all():
            v = 3
        f = 1 if user in proposal.followers.all() else 0
        p = 0
    return ControlForm(initial = { 'proposal': proposal.pk,
                         'vote': v,
                         'followed': f,
                         'poked': p })
