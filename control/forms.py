from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import ugettext_lazy as _
from django import forms
import logging

logger = logging.getLogger(__name__)

class ControlForm(forms.Form):
    proposal = forms.IntegerField(widget = forms.HiddenInput())

    vote = forms.IntegerField(widget = forms.HiddenInput())

    followed = forms.IntegerField(widget=forms.HiddenInput())

    poked = forms.IntegerField(widget=forms.HiddenInput())
