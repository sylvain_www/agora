from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect
from proposal.models import Proposal
from listing.views import list_tag
from agora.forms import ModifyForm
from .forms import AddForm
from .models import Tag
import logging
from django.db import transaction
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponseBadRequest
from django.contrib.auth.decorators import login_required
from agora.views import searchform

logger = logging.getLogger(__name__)

def shortcut(request, tag):
    t = get_object_or_404(Tag, pk=tag)
    return HttpResponseRedirect(reverse('tag:list',
                                   kwargs = { 'tag': tag,
                                              'slug': t.slug }))

def list(request, tag, slug):
    t = get_object_or_404(Tag, pk=tag)
    if slug != t.slug:
       return HttpResponseRedirect(reverse('tag:list',
                                       kwargs = { 'tag': tag,
                                                  'slug': t.slug }))
    return list_tag(request, t)

@login_required
def view(request, pk, slug):
    searchbox = searchform(request)
    p = get_object_or_404(Proposal, pk=pk)
    return render(request, 'tag/view.html', locals())

@login_required
def add(request, pk, slug):
    searchbox = searchform(request)
    p = get_object_or_404(Proposal, pk=pk)

    if request.method == 'POST':
        if 'add' in request.POST:
            form = AddForm(request.POST)
            if not form.is_valid():
                return render(request, 'tag/add.html', locals())

            with transaction.atomic():
                l = request.POST.get('label').lower()
                try:
                    t = Tag.objects.get(label=l)
                except ObjectDoesNotExist:
                    t = Tag.objects.create(label=l)
                p.tags.add(t)
                #Todo post a message with form.message

        return HttpResponseRedirect(reverse('proposal:tag:view',
                                   kwargs = { 'pk': pk, 'slug': slug }))

    form = AddForm()
    return render(request, 'tag/add.html', locals())

@login_required
def delete(request, pk, slug, tag):
    searchbox = searchform(request)
    p = get_object_or_404(Proposal, pk=pk)
    t = get_object_or_404(Tag, pk=tag)
    if not t in p.tags.all():
        return HttpResponseBadRequest("Bad request")

    if request.method == 'POST':
        if 'remove' in request.POST:
            form = ModifyForm(request.POST)
            if not form.is_valid():
                return render(request, 'tag/delete.html', locals())
            p.tags.remove(t)
            # TODO post message with form message

        return HttpResponseRedirect(reverse('proposal:tag:view',
                                   kwargs = { 'pk': pk, 'slug': slug }))

    form = ModifyForm()
    return render(request, 'tag/delete.html', locals())
