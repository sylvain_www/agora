from django.conf.urls import patterns, url

urlpatterns = patterns('tag.views',
    url(r'^$', 'view', name='view'),
    url(r'^a/$', 'add', name='add'),
    url(r'^d/(?P<tag>\d+)/$', 'delete', name='delete'))
