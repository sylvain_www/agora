from django.conf.urls import patterns, url

urlpatterns = patterns('tag.views',
    url(r'^(?P<slug>[-\w]+)/$', 'list', name='list'),
    url(r'^$', 'shortcut', name='shortcut'),
)
