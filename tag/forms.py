from django import forms
from agora.forms import ModifyForm
from django.utils.translation import ugettext_lazy as _
import logging

logger = logging.getLogger(__name__)

class AddForm(ModifyForm):
    label = forms.CharField(
            error_messages  = {'required':   _("Enter a tag."),
                               'min_length': _("The tag is too short (3 characters min)."),
                               'max_length': _("The tag is too long (50 characters max).")},
            min_length      = 3,
            max_length      = 50,
            required        = True,
            widget          = forms.TextInput({
            'placeholder': _("tag"),
            'size': 40 }))

    def clean_label(self):
        t = self.cleaned_data.get('message')
        t = " ".join(t.split())
        t = t.strip()
        if t == '':
            raise forms.ValidationError(_("Enter a valid tag."))
        return t
