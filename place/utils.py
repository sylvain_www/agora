from django.core.urlresolvers import reverse
from place.models import GenericPlace
import logging

logger = logging.getLogger(__name__)

def get_breadcrumbs(place):
    a = place.get_ancestors(include_self=True)
    b = []
    for l in a:
        if l.show:
            o = { 'label': l.label,
                  'url' : reverse('place:list', kwargs = {
                          'place': l.pk,
                          'slug': l.slug })}
            b.append(o)
    return b

