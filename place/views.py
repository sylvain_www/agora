from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.http import Http404
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from place.models import GenericPlace
import listing.views
import logging

logger = logging.getLogger(__name__)

def get_breadcrumbs(place):
    a = place.get_ancestors(include_self=True)
    b = []
    for l in a:
        if l.show:
            o = { 'label': l.label,
                  'url' : reverse('place:list', kwargs = {
                          'place': l.pk,
                          'slug': l.slug })}
            b.append(o)
    return b

def shortcut(request, place):
    l = get_object_or_404(GenericPlace, pk=place)
    if not l.show:
       raise Http404()
    return HttpResponseRedirect(reverse('place:list',
                                   kwargs = { 'place': place,
                                              'slug': l.slug }))

def list(request, place, slug):
    l = get_object_or_404(GenericPlace, pk=place)
    if not l.show:
       raise Http404()
    if slug != l.slug:
       return HttpResponseRedirect(reverse('place:list',
                                       kwargs = { 'place': place,
                                                  'slug': l.slug }))
    return listing.views.list_place(request, l)
