from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from germ.models import Germ
from germ.forms import NewGermForm
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from place.views import get_breadcrumbs
from django.views.defaults import server_error
from django.contrib.contenttypes.models import ContentType
from place.models import *
import logging
#import pdb; pdb.set_trace()

logger = logging.getLogger(__name__)

def save(form, user):
    pk = form.cleaned_data['germ_id']
    if pk == 0:
        g = Germ()
        g.creator = user
        g.version = 0
    else:
        g = get_object_or_404(Germ, pk=pk)
        g.version += 1
    g.title = form.cleaned_data['title']
    g.place = GenericPlace.objects.get(label=form.cleaned_data['place'])
    g.save()

@login_required
def new(request):
    if request.method == 'POST':
        i = request.POST.copy()
        form = NewGermForm(i)
        if 'validate' in request.POST:
            if form.is_valid():
                return HttpResponseRedirect(reverse('home'))

        elif 'save' in request.POST:
            if form.is_valid():
                save(form, request.user)
                return HttpResponseRedirect(reverse('germ:list'))

        else:
            return HttpResponseRedirect(reverse('home'))

        # if here, validation has failed, reset confim boxes
        if 'legit' in form.data: del form.data['legit']
        if 'etiquette' in form.data: del form.data['etiquette']
        if 'binary' in form.data: del form.data['binary']

    else:
        form = NewGermForm(initial={'germ_id': 0})

    return render(request, 'germ/edit.html', locals())

@login_required
def list(request):
    ps = Germ.objects.filter(creator = request.user)
    #u = request.user
    pg = Paginator(ps, 10)
    page = request.GET.get('page')

    try:
        pr = pg.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        pr = pg.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        pr = pg.page(pg.num_pages)

    ts = [ {'proposal': p,
            #'status': get_status(p, u),
            'breadcrumbs': get_breadcrumbs(p.place) } for p in pr ]
    return render(request, 'germ/list.html', locals())

@login_required
def edit(request, pk):
    g = get_object_or_404(Germ, pk=pk)
    if g.creator != request.user:
        return HttpResponseRedirect(reverse('germ:list'))

    form = NewGermForm(initial = { 'germ_id': pk,
                                   'title': g.title,
                                   'place': g.place })
    return render(request, 'germ/edit.html', locals())
