from django.conf.urls import patterns, url

urlpatterns = patterns('germ.views',
    url(r'^$', 'list', name='list'),
    url(r'^new/', 'new', name='new'),
    url(r'^(?P<pk>\d+)/', 'edit', name='edit')
)
