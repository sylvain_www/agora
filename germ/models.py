from django.contrib.auth.models import User
from django.utils import timezone
from django.db import models
from django.utils.translation import ugettext_lazy as _
from unidecode import unidecode
from place.models import GenericPlace
import logging

logger = logging.getLogger(__name__)

class Germ(models.Model):
    title = models.CharField(
        verbose_name    = _('title'),
        max_length      = 140,
        unique          = False,
        blank           = False,
        null            = False,
        help_text       = _('title'))

    creator = models.ForeignKey(
        to              = User,
        related_name    = 'germCreatorOf',
        editable        = False) # hide from admin and form

# SHOULD GO TO LOGS ONLY
#    verifiers = models.ManyToManyField(
#        to              = User,
#        through         = 'Verifier',
#        editable        = False) # hide from admin and form

    version = models.IntegerField(
        verbose_name    = _('version of the proposal'),
        editable        = False, # hide from admin and form
        default         = 0,
        help_text       = _('autopopulated'))

    place = models.ForeignKey(GenericPlace)

    def save(self, *args, **kwargs):
        if not self.id:
            # newly created object
            self.title = self.title[0:1].capitalize() + self.title[1:]

        self.full_clean()
        return super(Germ, self).save(*args, **kwargs)

    class Meta:
        verbose_name = _('germ')
        verbose_name_plural = _('germs')

    def __str__(self):
        return self.title
