from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import ugettext_lazy as _
from germ.models import Germ
from django import forms
from django.contrib.contenttypes.models import ContentType
import place.models
import logging
#import pdb; pdb.set_trace()

logger = logging.getLogger(__name__)

class NewGermForm(forms.Form):
    germ_id = forms.IntegerField(
            widget          = forms.HiddenInput())

    title = forms.CharField(
            label           = _("Title (10 to 140 characters)"),
            error_messages  = {'required':   _("Please choose a title."),
                               'min_length': _("The title is too short (10 characters min)."),
                               'max_length': _("The title is too long (140 characters max).")},
            min_length      = 10,
            max_length      = 140,
            help_text       = _("Choose wisely TODO"),
            required = True)

    place = forms.CharField(
            label           = _("Enter place, or city postcode"),
            max_length      = 50,
            required        = False)

    legit = forms.BooleanField(
            label           = _("The new proposal respects the legislation in place"),
            error_messages  = {'required': _("Please confirm the legislation is respected.")},
            help_text       = _("Avoid insulting, agressive or derogatory elements"),
            required        = True)

    etiquette = forms.BooleanField(
            label           = _("The new proposal respects the site etiquette"),
            error_messages  = {'required': _("Please confirm the etiquette is respected.")},
            help_text       = _("Use correct, simple and concise language"),
            required        = True)

    binary = forms.BooleanField(
            label           = _("One can vote for/against the new proposal"),
            error_messages  = {'required': _("Please confirm the proposal can be polled.")},
            help_text       = _('Formulate the title in a way that someone can say "I agree" or "I disagree" with this'),
            required        = True)

    def clean_title(self):
        t = self.cleaned_data.get('title')
        t = " ".join(t.split())
        t = t.strip()
        if t == '':
            logger.info("trying to enter an empty title")
            raise forms.ValidationError(_("Please enter a valid title."))
        if len(t) < 10:
            logger.info("trying to enter a title with less than 10 characters")
            raise forms.ValidationError(_("Please enter a longer title."))
        return t

    def clean_place(self):
        t = self.cleaned_data.get('place')
        t = " ".join(t.split())
        t = t.strip()
        if t == '':
            raise forms.ValidationError(_("Please enter a place."))
        l = place.models.clean_place(t)
        c = len(l)
        if c == 0:
            raise forms.ValidationError(_("No matching place was found, please check."))
        elif c > 1:
            raise forms.ValidationError(_("Multiple matches, please specify."))
        else:
            t = l[0]
            self.data['place'] = t
        return t

    class Meta:
        model = Germ
