��    2      �  C   <      H  3   I  &   }  -   �     �  7   �           >      N     o     �     �     �     �     �     �          
       
   /     :     O     _     n     }      �  	   �     �     �     �     �     �  
                  /     8     K     ^     s     �     �     �  
   �     �     �     �  	   �            3  .  3   b	  1   �	  1   �	     �	  G   
  #   _
     �
     �
     �
     �
     �
     �
     �
       
             %  	   <     F  3   [     �     �     �     �  '   �               "     +     3     H     T     a     m     �     �     �     �     �     �     �     �     �     �  '   �     $     2     >     Q            )                                &   -                    1   .   %            
               /                       #   $   	             (              ,   2         "      0                  !          '   *   +              %(user)s is %(age)s, and a member since %(joined)s. Last updated %(date)s by %(userlink)s. Proposal created by %(userlink)s on %(date)s. Proposal sponsored by There are no proposals matching the selected criterias. This proposal is deactivated. against against age categorya middle age person age categorya senior person age categorya teenager age categorya young person age categoryan elderly person child children clone clones discuss follow follower followers in favor in favor item items moderator moderators opposed opposed origin origins parent parents programme programmes proposal is recruting moderators reference related related share sign upcancel sign uprefresh list sign upsign up statistics synopsis undecided undecided unfollow user statsagainst user statscreator user statsfollowing user statsfor user statsmoderating user statssponsor user statsundecided verbclone verbsponsor visitor visitors vote: against vote: for vote: undecided we are recruiting moderators Project-Id-Version: 1.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-03-04 14:57+0100
PO-Revision-Date:  2015-03-03 13:24+0100
Last-Translator: Sylvain Killian <sylvain.killian@gmail.com>
Language-Team: 
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 %(user)s est %(age)s inscrite depuis le %(joined)s. Dernière mise à jour %(date)s par %(userlink)s. Proposition créée le %(date)s par %(userlink)s. Proposition sponsorisée par Il n'y a pas de proposition correspondant aux critères sélectionnés. Cette proposition est désactivée. contre contre une personne adulte une personne sénior une personne adolescente une jeune personne une personne agée fille filles clone clones discussion suivre abonné(e) abonné(e)s pour pour élément éléments modérateur/modératrice modérateurs/modératrices opposée opposées origine origines parente parentes programme programmes la proposition recrute des modérateurs référence similaire similaires partager annuler rafraîchir la liste inscription statistiques description indécis(e) indécis(es) ne plus suivre contre créateur/créatrice suivi pour modérateur/modératrice sponsor indécis(e) cloner sponsoriser visiteur/visiteuse visiteurs/visiteuses voter: contre voter: pour voter: indécis(e) nous recrutons des modérateurs 