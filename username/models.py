from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.contrib.auth.models import User
from django.db import transaction
from django.utils import timezone
from django.db.models import Q, Max
import datetime
import random
import logging
from django.utils.translation import ugettext_lazy as _
from username.settings import *
#import pdb; pdb.set_trace()

logger = logging.getLogger(__name__)

class Gender(models.Model):
    """Model to store the genders in this language."""
    key = models.CharField(
        verbose_name = _('short gender'),
        max_length   = 1,
        unique       = True,
        primary_key  = True,
        help_text    = _('short gender of the noun'))

    label = models.CharField(
        verbose_name = _('verbose gender'),
        max_length   = 12,
        unique       = True,
        help_text    = _('verbose gender of the noun'))

    class Meta:
        verbose_name = _('gender')
        verbose_name_plural = _('genders')

class Noun(models.Model):
    """Model to store the nouns and associated genders."""
    gender = models.ForeignKey(Gender)
    orthography = models.CharField(
        verbose_name = _('noun'),
        max_length   = 10,
        unique       = True,
        help_text    = _('orthography of the noun'))

    class Meta:
        verbose_name = _('noun')
        verbose_name_plural = _('nouns')

class Adjective(models.Model):
    """Model to store the adjectives in the various genders."""
    masculine = models.CharField(
        verbose_name = _('masculine'),
        max_length   = 10,
        unique       = True,
        help_text    = _('masculine form of the adjective'))
    feminine = models.CharField(
        verbose_name = _('feminine'),
        max_length   = 10,
        unique       = True,
        help_text    = _('feminine form of the adjective'))

    class Meta:
        verbose_name = _('adjective')
        verbose_name_plural = _('adjectives')

class Used(models.Model):
    """Model to store the pairs of noun/adjective already in use."""
    noun = models.ForeignKey(Noun)
    adjective = models.ForeignKey(Adjective)
    user = models.OneToOneField(User)
    reverse = models.BooleanField(
        verbose_name = _('invert noun and adjective'),
        default      = False,
        help_text    = _('a flag to set if noun and adjective '
                          'should be inverted'))

    class Meta:
        verbose_name = _('used')
        verbose_name_plural = _('used')

class Cached(models.Model):
    """Model to cache the next pairs noun/adjective to present during registration."""
    noun = models.ForeignKey(Noun)
    adjective = models.ForeignKey(Adjective)
    cachedNoun = models.CharField(
        verbose_name = _('cached noun orthography'),
        max_length   = 10,
        unique       = False,
        help_text    = _('a copy from noun table,'
                          ' for fast access'))
    cachedAdjective = models.CharField(
        verbose_name = _('cached adjective orthography'),
        max_length   = 10,
        unique       = False,
        help_text    = _('a copy from adjective '
                          'table, for fast access'))
    lock = models.IntegerField(
        verbose_name = _('user lock id'),
        default      = 0,
        help_text    = _('a per user lock integer id'))
    lockExpires = models.DateTimeField(
        verbose_name = _('date of expiry of lock id'),
        help_text    = _('lock id expire after a few minutes'))
    consumed = models.BooleanField(
        verbose_name = _('consumed item'),
        default      = False,
        help_text    = _('indicates if user rejected this item'))
    creationDate = models.DateTimeField(
        verbose_name = _('timestamp of creation of cache item'),
        help_text    = _('used to remove old entries from cache'))

    class Meta:
        verbose_name = _('cached')
        verbose_name_plural = _('cached')

def unlockCacheItems(lock_id):
    """Unlock matching items in the username cache.

    This method should be called when a user has selected
    a username, therefore unlocking all the usernames
    that were proposed to this user, and make them free to use
    for other users.
    """
    if lock_id == 0:
        logger.error("trying to remove lock on lock_id 0")
        return 0
    Cached.objects.filter(lock = lock_id).update(lock=0, consumed=False)
    logger.debug("unlock all items with lock_id %d", lock_id)

def unlockExpiredCacheItems():
    """Unlock expired but locked items in the username cache.

    Items may be locked (proposed during registration to the end user),
    but the registration was cancelled, left, or more simply another username
    was selected. After 5 minutes, these items are unlocked and made
    available for selection again.

    """
    Cached.objects.filter(~Q(lock = 0) &
       Q(lockExpires__lte = timezone.now())).update(lock = 0, consumed = False)
    logger.debug("unlocked expired items")

def removeOldCacheItems():
    """Remove items that are older than 5 days from the username cache.

    If a username has been in the cache for so many days, we just
    assume no one really wants to use that name. It may come back on
    a future refresh of the cache.
    """
    logger.debug("remove old cache items")
    Cached.objects.filter(Q(lock = 0) &
        Q(creationDate__lte = timezone.now() - datetime.timedelta(days=5))).delete()

def removeConsumedCacheItems():
    """Delete discarded items from the username cache, even if locked.

    Called from a transaction.
    """
    logger.warning("remove consumed cache items")
    O = Cached.objects.filter(consumed=True).delete()

def createCacheItems(requested_items = DEFAULT_CACHE_SIZE):
    """Create n new and unique cache items.

    Called from a transaction.

    Two algorithms are used to create a new username:
    1/ Random draw of noun + adjective
       This works efficiently when the number of free combinations
       are vastly greater than the number of usernames already booked
    2/ In case the first algorithm produces too many bad hits, switch
       to a second algorithm. It is less random, but concludes more
       quickly.
    """
    if requested_items > DEFAULT_CACHE_SIZE:
        logger.error("requesting creation of too many items")
        return
    n = Noun.objects.all().count()
    if n == 0:
        logger.critical("username db seems not initialised (no nouns)")
        return
    a = Adjective.objects.all().count()
    if a == 0:
        logger.critical("username db seems not initialised (no adjectives)")
        return
    m = Gender.objects.filter(pk='m').first()
    if m == None:
        logger.critical("username db seems not initialised (no genders)")
        return
    c = 0
    failed = 0
    t = timezone.now()
    d = datetime.timedelta(seconds = RANDOM_COLLISION_TIMEOUT)
    while c < requested_items:
        if failed >= RANDOM_COLLISION_TOLERANCE:
            # too many collisions (not enough free data), abort if too long
            if timezone.now() - t > d:
                logger.error("could not generate new usernames after %s",
                                RANDOM_COLLISION_TIMEOUT)
                return
        rn = random.randint(1, n)
        ra = random.randint(1, a)
        if Used.objects.filter(noun=rn, adjective=ra).exists():
            failed += 1
            continue
        if Cached.objects.filter(noun=rn, adjective=ra).exists():
            failed += 1
            continue
        N = Noun.objects.get(pk=rn)
        A = Adjective.objects.get(pk=ra)
        o = Cached(noun = N,
               adjective = A,
               cachedNoun = N.orthography,
               cachedAdjective = A.masculine if N.gender == m else A.feminine,
               lock = 0,
               lockExpires = timezone.now(),
               creationDate = timezone.now(),
               consumed = False)
        o.save()
        c += 1

def refreshCacheItems(requested_items = DEFAULT_CACHE_SIZE):
    """Replenish the username cache with the specified number of new items.

    Locked items are not removed. Old items are removed. If the random
    selection of new username fails after a few iterations, the algorithm
    switches to a more deterministic method, to ensure scalability.

    Return cache size after the operation.
    """
    logger.info("refreshing username cache")
    S = Cached.objects.all()
    if requested_items > DEFAULT_CACHE_SIZE:
        logger.error("requesting refresh of too many items")
        requested_items = DEFAULT_CACHE_SIZE

    with transaction.atomic():
        unlockExpiredCacheItems()
        removeOldCacheItems()
        if S.count() + requested_items > MAX_CACHE_SIZE:
            # cache is full
            removeConsumedCacheItems()
        if S.count() + requested_items > MAX_CACHE_SIZE:
            # cache is still full!
            # in this case, allow an empty username list to
            # be shown to the user, this will prompt a msg to
            # retry later
            logger.error("username cache is still full after cleanup procedure")
            return
        createCacheItems(requested_items)

def fetchCacheItems(lock_id = 0, size = MAX_FETCH_SIZE):
    """Book and return the next free items in the username cache.

    The lock id is allocated in case of a brand new registration
    ('first proposal'), or remains the same for an ongoing process
    ('refresh username').

    A cache refresh is triggered if the cache runs low.

    Return the selected items and lock id.
    """
    if size > MAX_FETCH_SIZE:
        logger.error("request too many items")
        return(0, [])

    e = timezone.now() + datetime.timedelta(minutes=5)
    with transaction.atomic():
        if lock_id:
            if not Cached.objects.filter(lock=lock_id).update(consumed=True):
                return (0, [])

        c = Cached.objects.filter(lock=0)

        if c.count() < CACHE_LOW:
            refreshCacheItems()
            if c.count() < size:
                logger.error("can't refresh cache with enough new items")
                return (0, [])

        l = lock_id if lock_id else Cached.objects.all().aggregate(Max('lock'))['lock__max'] + 1

        c = Cached.objects.filter(lock=0)[:size]
        r = []
        for o in c:
            o.lock = l
            o.lockExpires = e
            o.save()
            r.append((o.pk, o.cachedNoun + o.cachedAdjective))

    logger.debug("fetch %s objects with lock_id %s", size, l)
    r.sort()
    return (l, r)

def getUserCacheItems(lock_id):
    """Return the currently displayed items in the username cache.

    Return the selected items in an array compatible with choice
    selector.
    """
    if lock_id == 0:
        logger.error("get user cached items with null lock id")
        return []
    r = [ (o.pk, o.cachedNoun + o.cachedAdjective)
                for o in Cached.objects.filter(Q(lock = lock_id) &
                                               Q(consumed = False))]

    logger.debug("get objects with lock_id %d", lock_id)
    r.sort()
    return r

def getUsername(cache_pk, reverse):
    try:
        o = Cached.objects.get(pk=cache_pk)
        m = Gender.objects.get(pk='m')
    except ObjectDoesNotExist:
        return ""
    n = o.noun.orthography
    g = o.noun.gender
    a = o.adjective.masculine if g == m else o.adjective.feminine
    return a + n if reverse else n + a

def finaliseCacheItem(cache_pk, reverse, lock_id, user):
    if lock_id == 0 or user == None:
        return False
    with transaction.atomic():
        try:
            o = Cached.objects.get(lock = lock_id, pk = cache_pk)
        except ObjectDoesNotExist:
           logger.critical("try to finalise a user that is not booked")
           return False
        if Used.objects.filter(noun = o.noun, adjective = o.adjective).exists():
           logger.critical("try to finalise a user that is already used")
           return False
        if Used.objects.filter(user = user).exists():
           logger.critical("try to finalise a user that is already used 2")
           return False
        Used(noun = o.noun,
             adjective = o.adjective,
             reverse = reverse,
             user = user).save()
        o.delete()
        unlockCacheItems(lock_id)
    return True
