from django.test import TestCase
from username.models import *

class EmptyDatabaseTestCase(TestCase):
    def test_unlockCacheItems(self):
        unlockCacheItems(0)
        unlockCacheItems(1)

    def test_unlockExpiredCacheItems(self):
        unlockExpiredCacheItems()

    def test_refreshCacheItems(self):
        refreshCacheItems()
        self.assertEqual(Cached.objects.count(), 0)
        refreshCacheItems(1)
        self.assertEqual(Cached.objects.count(), 0)

    def test_fetchCacheItems(self):
        self.assertEqual(fetchCacheItems(), (0, []))
        self.assertEqual(fetchCacheItems(1), (0, []))

    def test_removeConsumedCacheItems(self):
        removeConsumedCacheItems()

    def test_createCacheItems(self):
        createCacheItems(0)
        createCacheItems(1)

    def test_getUserCacheItems(self):
        self.assertEqual(getUserCacheItems(0), [])
        self.assertEqual(getUserCacheItems(1), [])

    def test_getUsername(self):
        self.assertEqual(getUsername(0, False), "")
        self.assertEqual(getUsername(1, False), "")
        self.assertEqual(getUsername(1, True), "")

    def test_finaliseCacheItem(self):
        self.assertEqual(finaliseCacheItem(1, True, 0, None), False)
        self.assertEqual(finaliseCacheItem(0, False, 1, None), False)

class MethodsTestCase(TestCase):
    fixtures = ['fr_FR/gender.json', 'fr_FR/test/noun.json', 'fr_FR/test/adjective.json',
                    'test/user.json', 'fr_FR/test/used.json']

    def setUp(self):
        self.nounsCount = Noun.objects.all().count()
        self.adjectivesCount = Adjective.objects.all().count()
        self.defaultCacheSize = min(self.nounsCount*self.adjectivesCount, DEFAULT_CACHE_SIZE)

    def test_emptyCache(self):
        unlockCacheItems(2)
        self.assertEqual(Cached.objects.all().count(), 0)
        unlockExpiredCacheItems()
        self.assertEqual(Cached.objects.all().count(), 0)
        removeOldCacheItems()

    def test_cacheCorrectness(self):
        refreshCacheItems()
        self.assertEqual(Cached.objects.all().count(), self.defaultCacheSize)
        m = Gender.objects.get(label='masculine')
        f = Gender.objects.get(label='feminine')
        for o in Cached.objects.all():
            self.assertEqual(o.lock, 0)
            self.assertTrue(o.noun.gender == m or o.noun.gender == f)
            if o.noun.gender == m:
                self.assertEqual(o.cachedAdjective, o.adjective.masculine)
            else:
                self.assertEqual(o.cachedAdjective, o.adjective.feminine)

    def test_refreshCacheItems01(self):
        # empty cache at first
        self.assertEqual(Cached.objects.all().count(), 0)

        # populate 10 items in cache
        refreshCacheItems(10)
        self.assertEqual(Cached.objects.all().count(), 10)

        # add 14 items
        refreshCacheItems(14)
        self.assertEqual(Cached.objects.all().count(), 24)

        # add max items
        refreshCacheItems(self.defaultCacheSize)
        self.assertEqual(Cached.objects.all().count(), 24 + self.defaultCacheSize)

        # add incorrect number of items (more than actually supported)
        refreshCacheItems(max(self.nounsCount*self.adjectivesCount+1, self.defaultCacheSize+1))
        self.assertEqual(Cached.objects.all().count(), 24 + 2*self.defaultCacheSize)

    def test_refreshCacheItems02(self):
        # lock an item and refresh cache; test empty param
        refreshCacheItems(12)
        self.assertEqual(Cached.objects.all().count(), 12)
        o = Cached.objects.first()
        o.lock = 8
        o.lockExpires = timezone.now() + datetime.timedelta(minutes=5)
        o.save()
        refreshCacheItems()
        self.assertEqual(Cached.objects.all().count(), 12 + self.defaultCacheSize)
        self.assertEqual(Cached.objects.filter(lock=8).count(), 1)
        self.assertEqual(Cached.objects.all().count(), 12 + self.defaultCacheSize)

        # test refresh with expired locks in cache
        o.lockExpires = timezone.now() - datetime.timedelta(minutes=5)
        o.save()
        refreshCacheItems(1)
        self.assertEqual(Cached.objects.all().count(), 12 + self.defaultCacheSize + 1)
        self.assertEqual(Cached.objects.filter(lock=0).count(), 12 + self.defaultCacheSize + 1)
        self.assertEqual(Cached.objects.filter(lock=8).count(), 0)

        # test refresh with old entries in cache
        o = Cached.objects.first()
        o.creationDate = timezone.now() - datetime.timedelta(days=4)
        o.save()
        refreshCacheItems(1)
        self.assertEqual(Cached.objects.all().count(), 12 + self.defaultCacheSize + 1 + 1)

        # test refresh with old entry in cache
        o = Cached.objects.first()
        o.creationDate = timezone.now() - datetime.timedelta(days=6)
        o.save()
        refreshCacheItems(1)
        self.assertEqual(Cached.objects.all().count(), 12 + self.defaultCacheSize + 1 + 1 + 1 - 1)

        # test refresh with old, but locked entry in cache
        o = Cached.objects.first()
        o.lock = 9
        o.lockExpires = timezone.now() + datetime.timedelta(minutes=5)
        o.creationDate = timezone.now() - datetime.timedelta(days=6)
        o.save()
        refreshCacheItems(1)
        self.assertEqual(Cached.objects.all().count(), 12 + self.defaultCacheSize + 1 + 1 + 1 - 1 + 1)

        # test refresh with old and lock, but expired entry in cache
        o = Cached.objects.first()
        o.lock = 9
        o.lockExpires = timezone.now() - datetime.timedelta(minutes=5)
        o.creationDate = timezone.now() - datetime.timedelta(days=6)
        o.save()
        refreshCacheItems(1)
        self.assertEqual(Cached.objects.all().count(), 12 + self.defaultCacheSize + 1 + 1 + 1 - 1 + 1 + 1 - 1)

    def test_unlockCacheItems(self):
        refreshCacheItems(12)
        self.assertEqual(Cached.objects.all().count(), 12)
        o = Cached.objects.first()
        o.lock = 1
        o.lockExpires = timezone.now() + datetime.timedelta(minutes=5)
        o.save()
        self.assertEqual(Cached.objects.all().count(), 12)
        self.assertEqual(Cached.objects.filter(lock=0).count(), 11)
        self.assertEqual(Cached.objects.filter(lock=1).count(), 1)
        unlockCacheItems(2)
        self.assertEqual(Cached.objects.all().count(), 12)
        self.assertEqual(Cached.objects.filter(lock=0).count(), 11)
        self.assertEqual(Cached.objects.filter(lock=1).count(), 1)
        unlockCacheItems(1)
        self.assertEqual(Cached.objects.all().count(), 12)
        self.assertEqual(Cached.objects.filter(lock=0).count(), 12)
        self.assertEqual(Cached.objects.filter(lock=1).count(), 0)

    def test_unlockExpiredCacheItems(self):
        refreshCacheItems(10)
        self.assertEqual(Cached.objects.all().count(), 10)
        o = Cached.objects.filter(lock=0).first()
        o.lock = 9
        o.lockExpires = timezone.now() + datetime.timedelta(minutes=1)
        o.creationDate = timezone.now() - datetime.timedelta(days=6)
        o.save()
        self.assertEqual(Cached.objects.filter(~Q(lock=0)).count(), 1)
        unlockExpiredCacheItems()
        self.assertEqual(Cached.objects.all().count(), 10)
        self.assertEqual(Cached.objects.filter(~Q(lock=0)).count(), 1)
        o = Cached.objects.filter(lock=0).first()
        o.lock = 9
        o.lockExpires = timezone.now() - datetime.timedelta(minutes=1)
        o.save()
        self.assertEqual(Cached.objects.filter(~Q(lock=0)).count(), 2)
        unlockExpiredCacheItems()
        self.assertEqual(Cached.objects.all().count(), 10)
        self.assertEqual(Cached.objects.filter(~Q(lock=0)).count(), 1)

    def test_removeOldCacheItems(self):
        refreshCacheItems(10)
        self.assertEqual(Cached.objects.all().count(), 10)
        removeOldCacheItems()
        o = Cached.objects.filter(lock=0).first()
        o.creationDate = timezone.now() - datetime.timedelta(days=4)
        o.save()
        removeOldCacheItems()
        self.assertEqual(Cached.objects.all().count(), 10)
        o = Cached.objects.filter(lock=0).first()
        o.creationDate = timezone.now() - datetime.timedelta(days=6)
        o.save()
        removeOldCacheItems()
        self.assertEqual(Cached.objects.all().count(), 9)
        o = Cached.objects.filter(lock=0).first()
        o.lock = 3
        o.lockExpires = timezone.now() - datetime.timedelta(minutes=1)
        o.creationDate = timezone.now() - datetime.timedelta(days=6)
        o.save()
        removeOldCacheItems()
        self.assertEqual(Cached.objects.all().count(), 9)
        o = Cached.objects.filter(lock=3).first()
        o.lock = 0
        o.creationDate = timezone.now() - datetime.timedelta(days=6)
        o.save()
        removeOldCacheItems()
        self.assertEqual(Cached.objects.all().count(), 8)

    def test_fetchCacheItems(self):
        # ask more than allowed
        r = fetchCacheItems(0, MAX_FETCH_SIZE + 1)
        self.assertEqual(r, (0, []))

        # ask unknown previous lockId
        r = fetchCacheItems(4, MAX_FETCH_SIZE)
        self.assertEqual(r, (0, []))

        # test normal scenario
        (lockId, cache) = fetchCacheItems()
        self.assertEqual(len(cache), MAX_FETCH_SIZE)
        self.assertNotEqual(lockId, 0)
        self.assertEqual(Cached.objects.filter(lock=lockId).count(), MAX_FETCH_SIZE)

        # is lockId reused, and test smaller request
        (lockId2, cache) = fetchCacheItems(lockId, 4)
        self.assertEqual(len(cache), 4)
        self.assertEqual(lockId2, lockId)
        self.assertEqual(Cached.objects.filter(lock=lockId).count(), MAX_FETCH_SIZE + 4)

        # book another user
        (lockId3, cache) = fetchCacheItems(0, 6)
        self.assertEqual(len(cache), 6)
        self.assertNotEqual(lockId3, 0)
        self.assertNotEqual(lockId, lockId3)
        self.assertEqual(Cached.objects.filter(lock=lockId).count(), MAX_FETCH_SIZE + 4)
        self.assertEqual(Cached.objects.filter(lock=lockId3).count(), 6)

    def test_removeConsumedCacheItems(self):
        # cleanup when there is no item booked
        refreshCacheItems(self.defaultCacheSize)
        self.assertEqual(Cached.objects.count(), self.defaultCacheSize)
        removeConsumedCacheItems()
        self.assertEqual(Cached.objects.all().count(), self.defaultCacheSize)

        # cleanup when some items are booked but none free to remove
        (lockId, __) = fetchCacheItems(0, MAX_FETCH_SIZE)
        fetchCacheItems(0, MAX_FETCH_SIZE)
        fetchCacheItems(0, MAX_FETCH_SIZE)
        removeConsumedCacheItems()
        self.assertEqual(Cached.objects.all().count(), self.defaultCacheSize)

        # cleanup when some items can be removed
        fetchCacheItems(lockId, MAX_FETCH_SIZE)
        removeConsumedCacheItems()
        self.assertEqual(Cached.objects.all().count(), self.defaultCacheSize-MAX_FETCH_SIZE)

    def test_createCacheItems(self):
        # normal use case
        createCacheItems(DEFAULT_CACHE_SIZE)
        self.assertEqual(Cached.objects.all().count(), self.defaultCacheSize)

        # invalid request (request too many)
        createCacheItems(DEFAULT_CACHE_SIZE + 1)
        self.assertEqual(Cached.objects.all().count(), self.defaultCacheSize)

        # test performance with adverse conditions
        # fixture holds just above MAX_CACHE_SIZE possible usernames
        # first verify this assumption:
        n = Noun.objects.all().count()
        a = Adjective.objects.all().count()
        self.assertTrue(n*a < MAX_CACHE_SIZE + 15 * MAX_CACHE_SIZE / 100)
        # fill up cache except for one fill
        ni = 1
        ai = 1
        for i in range(0, MAX_CACHE_SIZE - Cached.objects.all().count() - self.defaultCacheSize):
            Cached(noun=Noun(pk=ni),
                   adjective=Adjective(pk=ai),
                   creationDate=timezone.now(), lockExpires=timezone.now()).save()
            ai += 1
            if ai > a:
                ai = 1
                ni += 1
        # now test if method can complete in an acceptable time frame:
        t = timezone.now()
        createCacheItems()
        d = timezone.now() - t
        m = datetime.timedelta(seconds=RANDOM_COLLISION_TIMEOUT+1)
        self.assertTrue(d < m, msg="test took too long to complete")

        # test if cache can overflow (it may)
        Cached.objects.all().delete()
        for i in range(0, MAX_CACHE_SIZE):
            Cached(noun=Noun(pk=1),
                   adjective=Adjective(pk=1),
                   creationDate=timezone.now(), lockExpires=timezone.now()).save()
        createCacheItems(1)
        self.assertEqual(Cached.objects.all().count(), MAX_CACHE_SIZE + 1)

    def test_getUserCacheItems(self):
        # normal use case
        (lockId, cache) = fetchCacheItems(0, MAX_FETCH_SIZE)
        (lockId2, cache2) = fetchCacheItems(0, MAX_FETCH_SIZE)
        self.assertEqual(set(getUserCacheItems(lockId)), set(cache))
        self.assertEqual(set(getUserCacheItems(lockId2)), set(cache2))
        (lockId, cache) = fetchCacheItems(lockId, MAX_FETCH_SIZE)
        self.assertEqual(set(getUserCacheItems(lockId)), set(cache))

        # check invalid inputs
        self.assertEqual(getUserCacheItems(0), [])
        self.assertEqual(getUserCacheItems(lockId2+1), [])

    def test_getUsername(self):
        from re import split as split
        refreshCacheItems()
        self.assertEqual(Cached.objects.count(), self.defaultCacheSize)

        # check invalid input
        missingpk = 1
        while Cached.objects.filter(pk=missingpk).count():
            missingpk += 1
        normal = getUsername(missingpk, False)
        self.assertEqual(normal, "")

        # get normal order username
        #import pdb; pdb.set_trace()
        normal = getUsername(Cached.objects.first().pk, False)
        self.assertNotEqual(normal, "")
        # check two capital letters in username
        ns = split('([A-Z])', normal)
        self.assertEqual(len(ns), 5)

        # same for reverse order username
        reverse = getUsername(Cached.objects.first().pk, True)
        self.assertNotEqual(reverse, "")
        rs = split('([A-Z])', reverse)
        self.assertEqual(len(rs), 5)

        # check reverse order
        self.assertEqual(ns[1], rs[3])
        self.assertEqual(ns[2], rs[4])
        self.assertEqual(ns[3], rs[1])
        self.assertEqual(ns[4], rs[2])

    def test_finaliseCacheItem(self):
        refreshCacheItems()
        self.assertEqual(Cached.objects.count(), self.defaultCacheSize)

        eu = User.objects.first()
        nu = User.objects.create_user(username = 'TotoTiti',
                                        email = 'toto@gmail.com',
                                        password = 'tototiti')
        nu2 = User.objects.create_user(username = 'TotoTiti2',
                                        email = 'toto2@gmail.com',
                                        password = 'tototiti')
        uc = Used.objects.all().count()

        # not booked
        r = finaliseCacheItem(1, True, 0, nu)
        self.assertEqual(r, False)
        self.assertEqual(Used.objects.all().count(), uc)

        # invalid lockId
        (lockId, cache) = fetchCacheItems(0, MAX_FETCH_SIZE)
        r = finaliseCacheItem(cache[0][0], True, lockId+1, nu)
        self.assertEqual(r, False)
        self.assertEqual(Used.objects.all().count(), uc)

        # invalid user
        r = finaliseCacheItem(cache[0][0], True, lockId, None)
        self.assertEqual(r, False)
        self.assertEqual(Used.objects.all().count(), uc)

        # invalid cache item
        r = finaliseCacheItem(1000, True, lockId, None)
        self.assertEqual(r, False)
        self.assertEqual(Used.objects.all().count(), uc)

        # invalid user (already used)
        r = finaliseCacheItem(cache[0][0], True, lockId, eu)
        self.assertEqual(r, False)
        self.assertEqual(Used.objects.all().count(), uc)

        # correct case
        r = finaliseCacheItem(cache[0][0], True, lockId, nu)
        self.assertEqual(r, True)
        self.assertEqual(Used.objects.all().count(), uc+1)
        self.assertTrue(Used.objects.filter(user=nu, reverse=True))

        # check cache state
        self.assertEqual(Cached.objects.count(), self.defaultCacheSize - 1)
        self.assertEqual(Cached.objects.filter(lock=lockId).count(), 0)

        # try to add reverse
        r = finaliseCacheItem(cache[0][0], False, lockId, nu2)
        self.assertEqual(r, False)
        self.assertEqual(Used.objects.all().count(), uc+1)

class UseCaseTestCase(TestCase):
    fixtures = ['fr_FR/gender.json', 'fr_FR/test/noun.json', 'fr_FR/test/adjective.json']

    def setUp(self):
        self.nounsCount = Noun.objects.all().count()
        self.adjectivesCount = Adjective.objects.all().count()
        self.defaultCacheSize = min(self.nounsCount*self.adjectivesCount, DEFAULT_CACHE_SIZE)

    def test_normalUseCase(self):
        # fetch some entries
        (lockId, cache) = fetchCacheItems(0, MAX_FETCH_SIZE)
        self.assertEqual(Cached.objects.all().count(), self.defaultCacheSize)
        self.assertEqual(len(cache), MAX_FETCH_SIZE)
        self.assertEqual(Cached.objects.filter(lock=lockId).count(), MAX_FETCH_SIZE)

        # fetch some more ('refresh')
        (lockId, cache) = fetchCacheItems(lock_id = lockId)
        self.assertEqual(Cached.objects.all().count(), self.defaultCacheSize)
        self.assertEqual(len(cache), MAX_FETCH_SIZE)
        self.assertEqual(Cached.objects.filter(lock=lockId).count(), 2*MAX_FETCH_SIZE)

        # another user starts registration
        (lockId2, cache) = fetchCacheItems(size = MAX_FETCH_SIZE)
        self.assertEqual(len(cache), MAX_FETCH_SIZE)
        self.assertEqual(Cached.objects.filter(lock=lockId2).count(), MAX_FETCH_SIZE)

        # first user wants some more ('refresh')
        (lockId, cache) = fetchCacheItems(lockId, MAX_FETCH_SIZE)
        self.assertEqual(len(cache), MAX_FETCH_SIZE)
        self.assertEqual(Cached.objects.filter(lock=lockId).count(), 3*MAX_FETCH_SIZE)

        # and unlock when done
        unlockCacheItems(lockId)
        self.assertEqual(Cached.objects.filter(lock=lockId).count(), 0)
        self.assertEqual(Cached.objects.filter(lock=lockId2).count(), MAX_FETCH_SIZE)
        unlockCacheItems(lockId2)
        self.assertEqual(Cached.objects.filter(lock=0).count(), self.defaultCacheSize)
        self.assertEqual(Cached.objects.filter(lock=lockId).count(), 0)
        self.assertEqual(Cached.objects.filter(lock=lockId2).count(), 0)

    def test_cacheLimitMultiUsers(self):
        # book all cache entries for different users
        # this will result in fetch to fail at some point
        # because - there is not enough test fixture to fill the cache quickly
        #         - or the cache gets full
        i = MAX_CACHE_SIZE / 10
        f = 0
        while i > 0:
            t = timezone.now()
            (lockId, cache) = fetchCacheItems(0, 10)
            d = timezone.now() - t
            self.assertTrue(d < datetime.timedelta(seconds=RANDOM_COLLISION_TIMEOUT+1), msg="test took too long to complete")
            if len(cache) != 10:
                break
            self.assertNotEqual(lockId, 0)
            self.assertEqual(Cached.objects.filter(lock=lockId).count(), 10)
            self.assertEqual(len(cache), 10)
            f += len(cache)
            i -= 1
        self.assertTrue(f == MAX_CACHE_SIZE or len(cache) == 0)
        self.assertEqual(Cached.objects.filter(~Q(lock=0)).count(), f)
        self.assertEqual(Cached.objects.filter(lock=0).count(), 0)

        # try and get even more
        if f < MAX_CACHE_SIZE:
            for i in range(0, MAX_CACHE_SIZE - f):
                Cached(noun = Noun(pk=1),
                       adjective = Adjective(pk=1),
                       lock = lockId+1+i,
                       creationDate = timezone.now(), lockExpires=timezone.now()).save()
        t = timezone.now()
        (lockId, cache) = fetchCacheItems(0, MAX_FETCH_SIZE)
        d = timezone.now() - t
        self.assertTrue(d < datetime.timedelta(seconds=RANDOM_COLLISION_TIMEOUT+1), msg="test took too long to complete")
        self.assertEqual(Cached.objects.all().count(), MAX_CACHE_SIZE)
        self.assertEqual(len(cache), 0)

    def test_cacheLimitSingleUser(self):
        # book all cache entries for one user
        # this should always work, since the cache
        # will clean automatically
        i = MAX_CACHE_SIZE / 10
        f = 0
        lockId = 0
        while i > 0:
            t = timezone.now()
            (lockId, cache) = fetchCacheItems(lockId, 10)
            d = timezone.now() - t
            self.assertTrue(d < datetime.timedelta(seconds=RANDOM_COLLISION_TIMEOUT+1), msg="test took too long to complete")
            self.assertEqual(lockId, 1)
            self.assertEqual(len(cache), 10)
            f += len(cache)
            self.assertEqual(Cached.objects.filter(lock=lockId).count(), f)
            self.assertEqual(Cached.objects.filter(lock=lockId).count(), f)
            i -= 1
        self.assertTrue(f == MAX_CACHE_SIZE)
        self.assertEqual(Cached.objects.filter(lock=0).count(), 0)
        self.assertEqual(Cached.objects.filter(lock=lockId).count(), MAX_CACHE_SIZE)

        # try and get even more
        t = timezone.now()
        (lockId, cache) = fetchCacheItems(1, MAX_FETCH_SIZE)
        d = timezone.now() - t
        self.assertTrue(d < datetime.timedelta(seconds=RANDOM_COLLISION_TIMEOUT+1), msg="test took too long to complete")
        self.assertEqual(len(cache), MAX_FETCH_SIZE)

    def test_allUsernamesUsed(self):
        pass

    def test_almostHalfUsernamesUsed(self):
        # should work 100%
        pass

    def test_almostAllUsernamesUsed(self):
        pass
