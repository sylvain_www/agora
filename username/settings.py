# number of entries that are repopulated in the cache
# when an update is requested without specifying
# a value
DEFAULT_CACHE_SIZE = 100

# maximum number of items in the cache; when this number
# is reached, some cleanup measures are triggered
MAX_CACHE_SIZE = 2000

# maximum number of items a client can book in one call
MAX_FETCH_SIZE = 10

# CACHE_LOW is the number of free items in the cache that
# will trigger an automatic cache update
CACHE_LOW = 10

# When generating new cache entries, we count the number of
# failed attemps by using a naive random pick of nouns and 
# adjectives. The failures should remain low as long as the 
# number of nouns*adjectives is high compared to the number
# of registered user. If too many failures are detected
# the routine bails out to avoid locking the database for
# too long.
RANDOM_COLLISION_TOLERANCE = 8

# max seconds before bail out
RANDOM_COLLISION_TIMEOUT = 5
