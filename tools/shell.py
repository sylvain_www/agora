#!/usr/bin/env python
import os
import sys
import django
import json
import code
import rlcompleter, readline

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "agora.settings")
    readline.parse_and_bind('tab:complete')
    django.setup()

    # here import default models
    from username.models import *
    from proposal.models import *
    from germ.models import *
    from account.models import *
    from place.models import *
    from agora.models import *

    try:
        p1 = Proposal.objects.get(pk=1)
        p2 = Proposal.objects.get(pk=2)
        u1 = User.objects.get(pk=2)
        u7 = User.objects.get(pk=7)
    except:
        pass

    code.interact(local=locals())
