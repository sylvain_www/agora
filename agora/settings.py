"""
Django settings for agora project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '%s55appat(isf=ow#7rk_==ab+a8@*8ihlv$i2%_+bp*0xqvi1'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

TEMPLATE_DIRS = [os.path.join(BASE_DIR, 'templates')]

ALLOWED_HOSTS = []

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    #'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'haystack',
    'mptt',
    'django_markdown',
    'agora',
    'place',
    'tag',
    'username',
    'account',
    'control',
    'cover',
    'synopsis',
    'listing',
    'proposal',
    'germ',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'agora.urls'

WSGI_APPLICATION = 'agora.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'devdb',
        'USER': 'django',
        'PASSWORD': 'django',
        'HOST': 'localhost',    # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '',
    }
}

# search
HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.solr_backend.SolrEngine',
        'URL': 'http://127.0.0.1:8080/solr',
        'INCLUDE_SPELLING': True,
        # ...or for multicore...
        # 'URL': 'http://127.0.0.1:8983/solr/mysite',
    },
}
HAYSTACK_DEFAULT_OPERATOR = 'AND'

# markdown
MARKDOWN_EXTENSIONS = ['abbr', 'footnotes', 'tables', 'nl2br', 'sane_lists', 'smarty']

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LOCALE_PATHS = [os.path.join(BASE_DIR, 'locale')]

LANGUAGE_CODE = 'fr-fr'

TIME_ZONE = 'Europe/Paris'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

# where to gather static files (ROOT)
STATIC_ROOT='/var/www/static'
# where to find them
STATIC_URL = '/static/'

MEDIA_ROOT = '/var/www/media/'
MEDIA_URL = '/media/'

# redirect to login url if login required:
LOGIN_URL = '/u/login/'
# redirect here if 'next' not specified:
LOGIN_REDIRECT_URL = '/a/'

# Logging
# adapted from https://docs.djangoproject.com/en/1.7/topics/logging/
#    DEBUG: Low level system information for debugging purposes
#    INFO: General system information
#    WARNING: Information describing a minor problem that has occurred.
#    ERROR: Information describing a major problem that has occurred.
#    CRITICAL: Information describing a critical problem that has occurred.

LOG_PATH = os.path.join(BASE_DIR, 'log')

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format': '%(asctime)s [%(levelname)s] <%(name)s:%(funcName)s()> %(message)s'
        },
        'verbose': {
            'format': '%(asctime)s [%(levelname)s] (%(process)d, 0x%(thread)x) <%(name)s> %(message)s'
        }
    },
    'filters': {
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        }
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'filters': ['require_debug_true'],
            'formatter': 'standard',
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            'filters': ['require_debug_false'],
            # not safe to send data via email, use sentry later:
            'include_html': True,
        },
        'file': {
            'level':'INFO',
            'class':'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(LOG_PATH, 'agora.log'),
            'maxBytes': 1024*1024*16,
            'backupCount': 10,
            'formatter':'standard',
        },
        'request_handler': {
            'level':'INFO',
            'class':'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(LOG_PATH, 'requests.log'),
            'maxBytes': 1024*1024*16,
            'backupCount': 10,
            'formatter':'standard',
        },
        'security_handler': {
            'level':'INFO',
            'class':'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(LOG_PATH, 'security.log'),
            'maxBytes': 1024*1024*16,
            'backupCount': 10,
            'formatter':'standard',
        },
    },
    'loggers': {
        # Django requests/security alerts go to specific files
        'django.request': {
            'handlers': ['console', 'mail_admins', 'request_handler', 'null'],
            'level': 'INFO',
            'propagate': False
        },
        'django.security': {
            'handlers': ['console', 'mail_admins', 'security_handler', 'null'],
            'level': 'INFO',
            'propagate': False
        },
        # top app settings (apps log events propagate to this)
        '': {
            'handlers': ['console', 'mail_admins', 'file', 'null'],
            'propagate': False,
        },
        # per app settings
        'django':                       { 'level': 'WARNING'   },
        'django.db.backends.schema':    { 'level': 'WARNING'   },
        'django.db.backends':           { 'level': 'WARNING'   },
        'username':                     { 'level': 'DEBUG'     },
        'register':                     { 'level': 'DEBUG'     },
        'germ':                         { 'level': 'DEBUG'     },
        'proposal':                     { 'level': 'DEBUG'     },
    },
}
