from django import forms
from django.utils.translation import ugettext_lazy as _
import logging

logger = logging.getLogger(__name__)

class ModifyForm(forms.Form):
    message = forms.CharField(
        error_messages  = {
        'required': _("Enter a reason for the change"),
        'min_length':
         _("The reason for the change is too short (10 characters minimum)."),
        'max_length':
        _("The reason for the change is too long (1000 characters maximum).")},
        min_length      = 10,
        max_length      = 1000,
        required        = True,
        widget          = forms.Textarea({
        'placeholder': _("Reason for change (10 characters minimum)"),
        'cols': 40,
        'rows': 4 }))

    def clean_message(self):
        t = self.cleaned_data.get('message')
        t = " ".join(t.split())
        t = t.strip()
        if t == '':
            raise forms.ValidationError(_("Enter a valid reason."))
        return t

