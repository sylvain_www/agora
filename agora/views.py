from django.shortcuts import render
from django.http import HttpResponseBadRequest
from haystack.forms import SearchForm
from haystack.query import SearchQuerySet
from proposal.models import Proposal
from tag.models import Tag
from place.models import GenericPlace
import listing.views
import pdb
from django import forms
from django.utils.translation import ugettext as _

def searchform(request):
    f = SearchForm(initial = {'models': {
                        'place.genericplace': True,
                        'tag.tag': True,
                        'proposal.proposal': True }})
    f.fields['q'].widget.attrs['placeholder'] = _('search')
    f.fields['q'].widget.attrs['size'] = 30
    return f

def search(request):
    f = SearchForm(request.GET)
    if not f.is_valid():
        return HttpResponseBadRequest("Bad request")

    r = SearchQuerySet().filter(content=request.GET.get('q'))
    pr = []
    for s in r.models(GenericPlace):
        pr.extend([ p for p in Proposal.objects.filter(place=s.object) ])
    for s in r.models(Tag):
        pr.extend([ p for p in Proposal.objects.filter(tags=s.object) ])
    pr.extend([ s.object for s in r.models(Proposal) ])
    return listing.views.list_search(request, pr)

def home(request):
    searchbox = searchform(request)
    return render(request, 'agora/home.html', locals())
