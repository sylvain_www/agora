from django.conf.urls import patterns, include, url
from django.contrib import admin
# for dev only:
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = patterns('',
    url(r'^$',  'agora.views.home', name='home'),
    url(r'^s/', 'agora.views.search', name='search'),
    url(r'^a/', include('listing.urls', namespace='listing')),
    url(r'^p/(?P<pk>\d+)/', include('proposal.urls', namespace='proposal')),
    url(r'^l/(?P<place>\d+)/', include('place.urls', namespace='place')),
    url(r'^t/(?P<tag>\d+)/', include('tag.urls', namespace='tag')),
    url(r'^u/', include('account.urls', namespace='account')),
    url(r'^g/', include('germ.urls', namespace='germ')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^markdown/$', 'synopsis.views.markdown', name='markdown'),
    url(r'^m/$', 'django_markdown.views.preview', name='django_markdown_preview')
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
