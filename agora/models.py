from django.db import models
from django.contrib.auth.models import User

class DeletedReason(models.Model):
    # 'charter'
    # 'spam'
    # 'double'
    # 'other'
    label = models.CharField(
        primary_key     = True,
        max_length      = 7)

    def __str__(self):
        return self.label

class DeletedContent(models.Model):
    reason = models.ForeignKey(
        to              = DeletedReason)

    user = models.ForeignKey(
        to              = User)

    date = models.DateTimeField(
        auto_now_add    = True)
