from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.http import HttpResponseBadRequest
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
import logging
from proposal.models import Proposal
from .models import Cover
from agora.forms import ModifyForm
from .forms import AddForm
from django.templatetags.static import static
from django.core.urlresolvers import reverse
from agora.models import DeletedContent, DeletedReason
import os
from django.db import transaction
from django.utils.translation import ugettext as _
from agora.views import searchform

logger = logging.getLogger(__name__)

@login_required
def view(request, pk, slug):
    searchbox = searchform(request)
    p = get_object_or_404(Proposal, pk=pk)
    c = get_object_or_404(Cover, pk=p.cover_root.pk)
    covers = c.get_descendants(include_self=True)
    return render(request, 'cover/view.html', locals())

@login_required
def select(request, pk, slug, cover):
    searchbox = searchform(request)
    p = get_object_or_404(Proposal, pk=pk)
    c = get_object_or_404(Cover, pk=cover)
    if (c.tree_id != p.cover_root.tree_id or
        c.deleted or p.cover == c):
        return HttpResponseBadRequest("Bad request")

    if request.method == 'POST':
        if 'select' in request.POST:
            form = ModifyForm(request.POST)
            if not form.is_valid():
                return render(request, 'cover/select.html', locals())

            with transaction.atomic():
                p.cover = c
                p.save()
            # todo: publish message

        return HttpResponseRedirect(reverse('proposal:cover:view',
                               kwargs = { 'pk': pk, 'slug': slug }))

    form = ModifyForm()
    return render(request, 'cover/select.html', locals())

@login_required
def add(request, pk, slug):
    searchbox = searchform(request)
    p = get_object_or_404(Proposal, pk=pk)

    if request.method == 'POST':
        if 'add' in request.POST:
            form = AddForm(request.POST, request.FILES)
            if not form.is_valid():
                return render(request, 'cover/add.html', locals())

            with transaction.atomic():
                c = get_object_or_404(Cover, pk=p.cover_root.pk)
                n = Cover(picture = request.FILES['picture'],
                          creator = request.user)

                o = c.get_descendants(include_self=True).last()
                o = get_object_or_404(Cover, pk=o.pk)
                n.insert_at(o, save=True)
                p.cover = n
                p.save()

        return HttpResponseRedirect(reverse('proposal:cover:view',
                               kwargs = { 'pk': pk, 'slug': slug }))

    form = AddForm()
    return render(request, 'cover/add.html', locals())

@login_required
def delete(request, pk, slug, cover):
    searchbox = searchform(request)
    p = get_object_or_404(Proposal, pk=pk)
    c = get_object_or_404(Cover, pk=cover)
    if (c.tree_id != p.cover_root.tree_id or
        c == p.cover_root or
        c.deleted):
        return HttpResponseBadRequest("Bad request")

    if request.method == 'POST':
        if 'delete' in request.POST:
            form = ModifyForm(request.POST)
            if not form.is_valid():
                return render(request, 'cover/delete.html', locals())

            with transaction.atomic():
                if p.cover == c:
                    p.cover = p.cover_root
                    p.save()
                r = DeletedReason.objects.get(label='other') #todo
                c.deleted = DeletedContent.objects.create(
                                reason = r,
                                user   = request.user)
                c.save()
                # todo: publish message

        return HttpResponseRedirect(reverse('proposal:cover:view',
                               kwargs = { 'pk': pk, 'slug': slug }))

    form = ModifyForm()
    return render(request, 'cover/delete.html', locals())
