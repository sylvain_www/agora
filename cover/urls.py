from django.conf.urls import patterns, include, url

urlpatterns = patterns('cover.views',
    url(r'^$', 'view', name='view'),
    url(r'^a/$', 'add', name='add'),
    url(r'^s/(?P<cover>\d+)/$', 'select', name='select'),
    url(r'^d/(?P<cover>\d+)/$', 'delete', name='delete'))
