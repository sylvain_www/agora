from django import forms
from agora.forms import ModifyForm
from django.utils.translation import ugettext_lazy as _
import logging

logger = logging.getLogger(__name__)

class AddForm(ModifyForm):
    picture = forms.ImageField(required=True)
