from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from mptt.models import MPTTModel, TreeForeignKey
from agora.models import DeletedContent
from PIL import Image, ImageOps
import os
import uuid
import re

def get_picture_path(self, filename):
    m = re.match('.*\.([a-zA-Z0-9]{2,8})$', filename)
    u = uuid.uuid4()
    if m:
        f = "%s.%s" % (u, m.groups()[0])
    else:
        f = "%s" % (u)
    return os.path.join('c', f[:1], f[1:2], f[2:])

class Cover(MPTTModel):
    parent = TreeForeignKey('self',
        null            = True,
        blank           = True,
        related_name    = 'children')

    picture = models.ImageField(
        upload_to       = get_picture_path)

    creator = models.ForeignKey(
        to              = User,
        related_name    = 'covers')

    creation_date = models.DateTimeField(
        auto_now_add    = True)

    deleted = models.OneToOneField(
        to              = DeletedContent,
        null            = True,
        blank           = True)

    def save(self, *args, **kwargs):
        super(Cover, self).save(*args, **kwargs)

        if self.picture and (
                self.picture.height != 150 or
                self.picture.width != 150):
            with Image.open(self.picture.path) as image:
                if image.mode not in ("L", "RGB"):
                    image = image.convert("RGB")
                image = ImageOps.fit(image, (150, 150), Image.ANTIALIAS)
                image.save(self.picture.path)
