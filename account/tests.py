from django.contrib.auth.models import User
from django.test import TestCase
from django.core.urlresolvers import reverse
from account.forms import RegisterForm
from django.utils import timezone
from username.models import getUserCacheItems
from place.models import *
import datetime

class EmptyDatabaseTestCase(TestCase):
    default_cache = [(2, 'item2'), (3, 'item3'), (4, 'item4'), (5, 'item5'), (6, 'item6')]
    default_data = {'lockId': 1, 'username': 2, 'reverse': False, 'email': 'foo@bar.com',
                    'password1': 'password', 'password2': 'password',
                    'birth': '1977', 'place': 'paris'}

    def test_register(self):
        # going to register page fail elegantly
        response = self.client.get(reverse('account:register'))
        self.assertEqual(response.status_code, 500)

        # posts fail elegantly too
        form_data = dict(self.default_data)
        response = self.client.post(reverse('account:register'), data = form_data)
        self.assertRedirects(response, reverse('home'), status_code=302, target_status_code=200)

        form_data = dict(self.default_data)
        form_data['refresh'] = ''
        response = self.client.post(reverse('account:register'), data = form_data)
        self.assertEqual(response.status_code, 500)

        form_data = dict(self.default_data)
        form_data['cancel'] = ''
        response = self.client.post(reverse('account:register'), data = form_data)
        self.assertRedirects(response, reverse('home'), status_code=302, target_status_code=200)

        form_data = dict(self.default_data)
        form_data['validate'] = ''
        response = self.client.post(reverse('account:register'), data = form_data)
        self.assertEqual(response.status_code, 500)

class formTestCase(TestCase):
    fixtures = ['fr_FR/gender.json', 'fr_FR/test/noun.json', 'fr_FR/test/adjective.json', 'test/user.json', 'fr_FR/test/used.json', 'interregion.json', 'country.json' ]
    default_cache = [(2, 'item2'), (3, 'item3'), (4, 'item4'), (5, 'item5'), (6, 'item6')]
    default_data = {'lockId': 1, 'username': 2, 'reverse': False, 'email': 'foo@bar.com',
                    'password1': 'password', 'password2': 'password',
                    'birth': '1977', 'place_selector': 'co'}

    def test_pass(self):
        form_data = dict(self.default_data)
        form = RegisterForm(self.default_cache, data = form_data)
        #import pdb; pdb.set_trace()
        t = form.is_valid()
        self.assertEqual(form.is_valid(), True)

        # test reverse
        form_data = dict(self.default_data)
        form_data['reverse'] = True
        form = RegisterForm(self.default_cache, data = form_data)
        self.assertEqual(form.is_valid(), True)

        # test funky emails
        form_data = dict(self.default_data)
        form_data['email'] = 'test+!.#$%@toTo.co.uk'
        form = RegisterForm(self.default_cache, data = form_data)
        self.assertEqual(form.is_valid(), True)

        # test funky password
        form_data = dict(self.default_data)
        form_data['password1'] = """!@#$%^&*()_-+=][{}"':;<>.,?|/\fsafsfsf~`"""
        form_data['password2'] = """!@#$%^&*()_-+=][{}"':;<>.,?|/\fsafsfsf~`"""
        form = RegisterForm(self.default_cache, data = form_data)
        self.assertEqual(form.is_valid(), True)

    def test_limits(self):
        # birth limits
        form_data = dict(self.default_data)
        form_data['birth'] = '1900'
        form = RegisterForm(self.default_cache, data = form_data)
        self.assertEqual(form.is_valid(), True)

        form_data = dict(self.default_data)
        form_data['birth'] = '1899'
        form = RegisterForm(self.default_cache, data = form_data)
        self.assertEqual(form.is_valid(), False)

        form_data = dict(self.default_data)
        form_data['birth'] = str(datetime.date(timezone.now().year-14, 1, 1).year)
        form = RegisterForm(self.default_cache, data = form_data)
        self.assertEqual(form.is_valid(), True)

        form_data = dict(self.default_data)
        form_data['birth'] = str(datetime.date(timezone.now().year-13, 1, 1).year)
        form = RegisterForm(self.default_cache, data = form_data)
        self.assertEqual(form.is_valid(), False)

        # short password
        form_data = dict(self.default_data)
        form_data['password1'] = '1234567'
        form_data['password2'] = '1234567'
        form = RegisterForm(self.default_cache, data = form_data)
        self.assertEqual(form.is_valid(), False)

    def test_failures(self):
        # incorrect username
        form_data = dict(self.default_data)
        form_data['username'] = 7
        form = RegisterForm(self.default_cache, data = form_data)
        self.assertEqual(form.is_valid(), False)

        # incorrect lockId
        form_data = dict(self.default_data)
        form_data['lockId'] = 0
        form = RegisterForm(self.default_cache, data = form_data)
        self.assertEqual(form.is_valid(), False)

        form_data = dict(self.default_data)
        form_data['lockId'] = -1
        form = RegisterForm(self.default_cache, data = form_data)
        self.assertEqual(form.is_valid(), False)

        # different passwords
        form_data = dict(self.default_data)
        form_data['password1'] = '12345678'
        form_data['password2'] = '12345679'
        form = RegisterForm(self.default_cache, data = form_data)
        self.assertEqual(form.is_valid(), False)

    def test_emptyFields(self):
        # empty username
        form_data = dict(self.default_data)
        del form_data['username']
        form = RegisterForm(self.default_cache, data = form_data)
        self.assertEqual(form.is_valid(), False)

        # empty lockId
        form_data = dict(self.default_data)
        del form_data['lockId']
        form = RegisterForm(self.default_cache, data = form_data)
        self.assertEqual(form.is_valid(), False)

        # empty reverse
        form_data = dict(self.default_data)
        del form_data['reverse']
        form = RegisterForm(self.default_cache, data = form_data)
        self.assertEqual(form.is_valid(), True)
        self.assertEqual(form.cleaned_data['reverse'], False)

        # empty email
        form_data = dict(self.default_data)
        del form_data['email']
        form = RegisterForm(self.default_cache, data = form_data)
        self.assertEqual(form.is_valid(), False)

        # empty pass1
        form_data = dict(self.default_data)
        del form_data['password1']
        form = RegisterForm(self.default_cache, data = form_data)
        self.assertEqual(form.is_valid(), False)

        # empty pass2
        form_data = dict(self.default_data)
        del form_data['password2']
        form = RegisterForm(self.default_cache, data = form_data)
        self.assertEqual(form.is_valid(), False)

        # empty birth
        form_data = dict(self.default_data)
        del form_data['birth']
        form = RegisterForm(self.default_cache, data = form_data)
        self.assertEqual(form.is_valid(), False)

        # empty place
        form_data = dict(self.default_data)
        del form_data['place_selector']
        form = RegisterForm(self.default_cache, data = form_data)
        self.assertEqual(form.is_valid(), False)

class useCaseTestCase(TestCase):
    fixtures = ['fr_FR/gender.json', 'fr_FR/test/noun.json', 'fr_FR/test/adjective.json', 'test/user.json', 'fr_FR/test/used.json', 'interregion.json', 'country.json' ]
    default_cache = [(2, 'item2'), (3, 'item3'), (4, 'item4'), (5, 'item5'), (6, 'item6')]
    default_data = {'lockId': 1, 'username': 2, 'reverse': False, 'email': 'foo@bar.com',
                    'password1': 'password', 'password2': 'password',
                    'birth': '1977', 'place_selector': 'co'}

    def test_postWithoutPriorUsernameBooking(self):
        # validate
        form_data = dict(self.default_data)
        form_data['validate'] = ''
        response = self.client.post(reverse('account:register'), data = form_data)
        self.assertEqual(response.status_code, 500)

        # refresh
        form_data = dict(self.default_data)
        form_data['refresh'] = ''
        response = self.client.post(reverse('account:register'), data = form_data)
        self.assertEqual(response.status_code, 500)

        # cancel
        form_data = dict(self.default_data)
        form_data['cancel'] = ''
        response = self.client.post(reverse('account:register'), data = form_data)
        self.assertRedirects(response, reverse('home'), status_code=302, target_status_code=200)

    def test_normalFlow(self):
        response = self.client.get(reverse('account:register'))
        self.assertEqual(response.status_code, 200)

        # refresh
        form_data = dict(self.default_data)
        form_data['refresh'] = ''
        response = self.client.post(reverse('account:register'), data = form_data)
        self.assertEqual(response.status_code, 200)

        # cancel
        form_data = dict(self.default_data)
        form_data['cancel'] = ''
        response = self.client.post(reverse('account:register'), data = form_data)
        self.assertRedirects(response, reverse('home'), status_code=302, target_status_code=200)

        response = self.client.get(reverse('account:register'))
        self.assertEqual(response.status_code, 200)

        # validate
        nu = User.objects.all().count()
        form_data = dict(self.default_data)
        form_data['validate'] = ''
        c = getUserCacheItems(1)
        form_data['username'] = c[0][0]
        response = self.client.post(reverse('account:register'), data = form_data)
        self.assertRedirects(response, reverse('home'), status_code=302, target_status_code=200)
        self.assertEqual(User.objects.all().count(), nu + 1)

        response = self.client.get(reverse('account:register'))
        self.assertEqual(response.status_code, 200)

        # validate same email
        form_data = dict(self.default_data)
        form_data['validate'] = ''
        c = getUserCacheItems(1)
        form_data['username'] = c[0][0]
        response = self.client.post(reverse('account:register'), data = form_data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(User.objects.all().count(), nu + 1)

        # update email
        form_data['email'] = 'newemail@test.com'
        response = self.client.post(reverse('account:register'), data = form_data)
        self.assertRedirects(response, reverse('home'), status_code=302, target_status_code=200)
        self.assertEqual(User.objects.all().count(), nu + 2)
