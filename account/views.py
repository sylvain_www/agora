from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.utils.translation import pgettext
from django.utils import timezone
from account.models import Account
from username.models import fetchCacheItems, unlockCacheItems, getUserCacheItems, finaliseCacheItem, getUsername
from account.forms import RegisterForm
from django.views.defaults import server_error
from place.models import GenericPlace
import logging
#import pdb; pdb.set_trace()

logger = logging.getLogger(__name__)
def agecategory(birthyear):
    """Return an age category based on the provided birth date."""
    age = timezone.now().year - birthyear
    if age < 18:
        return 1
    if age < 25:
        return 2
    if age < 50:
        return 3
    if age < 68:
        return 4
    return 5

age_categories = { 1: pgettext('age category', 'a teenager'),
                   2: pgettext('age category', 'a young person'),
                   3: pgettext('age category', 'a middle age person'),
                   4: pgettext('age category', 'a senior person'),
                   5: pgettext('age category', 'an elderly person') }

@login_required
def shortcut(request, pk):
   u = get_object_or_404(User, pk=pk)
   return HttpResponseRedirect(reverse('account:userview',
                                   kwargs = { 'username': u.username }))

@login_required
def user_view(request, username):
    u = get_object_or_404(User, username=username)
    pk = u.pk
    age = pgettext('age category', age_categories[agecategory(u.account.birth)])
    ss = []
    for s in [ { 'label':   pgettext('user stats', 'creator'),
                 'field':   u.creatorOf,
                 'param':   'uc',
                 'hideIf0': True  },
               { 'label':   pgettext('user stats', 'sponsor'),
                 'field':   u.sponsorOf,
                 'param':   'us',
                 'hideIf0': True  },
               { 'label':   pgettext('user stats', 'following'),
                 'field':   u.follower,
                 'param':   'ug',
                 'hideIf0': True  },
               #{ 'label':   pgettext('user stats', 'moderating'),
               #  'field':   u.moderatorOf,
               #  'param':   'um',
               #  'hideIf0': True  },
               { 'label':   pgettext('user stats', 'for'),
                 'field':   u.infavor,
                 'param':   'uf',
                 'hideIf0': False },
               { 'label':   pgettext('user stats', 'against'),
                 'field':   u.against,
                 'param':   'ua',
                 'hideIf0': False },
               { 'label':   pgettext('user stats', 'undecided'),
                 'field':u.undecided,
                 'param':'uu',
                 'hideIf0': False }]:
        c = s['field'].all().count()
        if not c and s['hideIf0']:
            continue
        if c == 1:
            p = s['field'].first()
            url = reverse('proposal:content', kwargs = {'pk': p.pk, 'slug': p.slug})
        else:
            url = "%s?%s=%s" % (reverse('listing:standard'), s['param'], pk)
        ss.append({'label': s['label'], 'cnt': c, 'url': url})
    extend = 'agora/content.html'
    return render(request, 'account/user.html', locals())

def validateRegister(request, data):
    username = getUsername(data['username'], data['reverse'])
    if not username:
        logger.critical("could not get username label")
        return server_error(request)

    try:
        u = User.objects.create_user(username = username,
                                     email = data['email'],
                                     password = data['password1'])
    except:
        logger.critical("could not create user")

    try:
        p = GenericPlace.objects.get(label=data['place'])
        s = Account.objects.create(user = u,
                                      birth = data['birth'].year,
                                      place = p)
    except:
        logger.critical("could not create account")
        u.delete()
        return server_error(request)

    if not finaliseCacheItem(data['username'], data['reverse'], data['lockId'], u):
        logger.critical("some error while registering new user")
        s.delete()
        u.delete()
        unlockCacheItems(data['lockId'])
        return server_error(request)

    return HttpResponseRedirect(reverse('home'))

def register(request):
    if request.method == 'POST':
        try:
            l = int(request.POST['lockId'])
        except:
            logger.error("incorrect or missing lockId in POST data")
            return HttpResponseRedirect(reverse('home'))

        if 'validate' in request.POST:
            c = getUserCacheItems(l)
            if not c:
                logger.critical("could not retrieve previously locked items")
                return server_error(request)
            i = request.POST.copy()
            form = RegisterForm(c, i)
            # reinit selector with current value
            if form.is_valid():
                return validateRegister(request, form.cleaned_data)

        elif 'refresh' in request.POST:
            (l, c) = fetchCacheItems(l)
            if not l:
                logger.critical("could not fetch items")
                return server_error(request)
            i = request.POST.copy()
            i['username'] = c[0][0]
            form = RegisterForm(c, initial = i)

        else:
            unlockCacheItems(l)
            return HttpResponseRedirect(reverse('home'))

    else:
        (l, c) = fetchCacheItems()
        if not l:
            logger.critical("could not fetch items")
            return server_error(request)
        form = RegisterForm(c, initial = {'lockId': l,
                                          'username': c[0][0]})

    extend = "agora/flat_popup.html"
    #when browser says javascript is on:
    #extend = "agora/popup.html"
    return render(request, 'account/register.html', locals())

#    return HttpResponse(json.dumps(cache), content_type='application/json')
