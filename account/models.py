from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from place.models import GenericPlace

class Account(models.Model):
    user = models.OneToOneField(User)

    birth = models.IntegerField(
        verbose_name    = _('year of birth'),
        editable        = False, # hide from admin and form
        default         = 0,
        help_text       = _('year of birth'))

    place = models.ForeignKey(GenericPlace)
