from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    url(r'^login/$', 'django.contrib.auth.views.login',
            {'template_name': 'account/login.html'}, name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout',
            {'next_page': 'home'}, name='logout'),
    url(r'^register/$', 'account.views.register', name='register'),
    url(r'^(?P<pk>\d+)/', 'account.views.shortcut', name='shortcut'),
    url(r'^(?P<username>\w+)/$', 'account.views.user_view', name='userview'),
)
