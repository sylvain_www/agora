from django.contrib.auth.models import User
from django.utils import timezone
import datetime
import logging
from django.utils.translation import ugettext_lazy as _
from django import forms
from django.contrib.contenttypes.models import ContentType
import place.models
#import pdb; pdb.set_trace()

logger = logging.getLogger(__name__)

class RegisterForm(forms.Form):
    error_css_class = 'error'
    required_css_class = 'required'

    def __init__(self, cache, *args, **kwargs):
        super(RegisterForm, self).__init__(*args, **kwargs)
        self.fields['username'] = forms.ChoiceField(
            label           = _("Pick a username"),
            help_text       = _("In you don't like any, hit that refresh button!"),
            error_messages  = {'required':       _("Pick a username from the list."),
                               'invalid_choice': _("Pick a username from the list.")},
            widget          = forms.RadioSelect,
            required        = True,
            choices         = cache)

    lockId = forms.IntegerField(
            min_value       = 1,
            widget          = forms.HiddenInput())

    reverse = forms.BooleanField(
            label           = _("Swap words?"),
            required        = False)

    email = forms.EmailField(
            label           = _("Email adress"),
            error_messages  = {'required': _("Enter your email."),
                               'invalid':  _("The email you entered is not valid.")},
            required=True)

    password1 = forms.CharField(
            label           = _("Choose a password"),
            error_messages  = {'required':   _("Choose a password."),
                               'max_length': _("The password is too long."),
                               'min_length': _("The password should be 8 characters minimum.")},
            min_length      = 8,
            max_length      = 50,
            widget          = forms.PasswordInput,
            required        = True)

    password2 = forms.CharField(
            label           = _("Repeat the password"),
            error_messages  = {'required':   _("Repeat the password."),
                               'max_length': _("The password is too long."),
                               'min_length': _("The password should be 8 characters minimum.")},
            min_length      = 8,
            max_length      = 50,
            widget          = forms.PasswordInput,
            required        = True)

    birth = forms.DateField(
            label           = _("Enter your birth year"),
            error_messages  = {'required': _("Enter your birth year."),
                               'invalid':  _("The birth year you entered is not valid.")},
            input_formats   = [ '%Y' ],
            required        = True)

    place = forms.CharField(
            label           = _("Enter place, or city postcode"),
            help_text       = _("Any place name, or city postcode"),
            max_length      = 50,
            required        = True)

    def clean_email(self):
        email = self.cleaned_data.get('email')
        if User.objects.filter(email__iexact=email).exists():
            # Translators: error message when registering an already used email
            raise forms.ValidationError(_("An account with this email already exists."))
        return email

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password1 != password2:
            raise forms.ValidationError(_("Passwords do not match."))
        return password2

    def clean_birth(self):
        birth = self.cleaned_data.get('birth')
        minDate = datetime.date(1900, 1, 1)
        maxDate = datetime.date(timezone.now().year-14, 1, 1)
        if birth < minDate:
            raise forms.ValidationError(_("This does not seem to be correct."))
        if birth > maxDate:
            raise forms.ValidationError(_("You must be 14 years old to sign up."))
        return birth

    def clean_place(self):
        t = self.cleaned_data.get('place')
        if t == '':
            raise forms.ValidationError(_("Please enter a place."))
        l = place.models.clean_place(t)
        c = len(l)
        if c == 0:
            raise forms.ValidationError(_("No matching place was found, please check."))
        elif c > 1:
            raise forms.ValidationError(_("Multiple matches, please specify."))
        else:
            t = l[0]
            self.data['place'] = t
        return t
