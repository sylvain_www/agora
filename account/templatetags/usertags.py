from django import template
from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe

register = template.Library()

@register.tag(name="getuserlink")
def do_userlink(parser, token):
    try:
        tag_name, user, proposal = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError(
            "%r tag requires 2 arguments" % token.contents.split()[0])
    return userlinkNode(user, proposal)

@register.tag
class userlinkNode(template.Node):
    def __init__(self, user, proposal):
        self.user = template.Variable(user)
        self.proposal = template.Variable(proposal)

    def render(self, context):
        try:
            u = self.user.resolve(context)
            p = self.proposal.resolve(context)
            n = u.username
            url = reverse('account:userview', kwargs = { 'username': n })
            if p in u.infavor.all():
                a = 'infavor'
            elif p in u.against.all():
                a = 'against'
            elif p in u.undecided.all():
                a = 'undecided'
            else:
                a = 'novote'
            context['userlink'] = mark_safe('<a class="%s" href="%s">%s</a>' % (a, url, n))
        except template.VariableDoesNotExist:
            context['userlink'] = '???'
        return ''

