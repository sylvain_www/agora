from django.shortcuts import render
from django.http import HttpResponseBadRequest
from django.shortcuts import redirect
from proposal.models import Proposal
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
import operator
from django.db.models import Q
from django.utils.translation import ungettext
from functools import reduce
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from place.utils import get_breadcrumbs
from control.views import get_control_form, apply_control_form
from place.utils import get_breadcrumbs
import agora.views
import logging

logger = logging.getLogger(__name__)

def common(request):
    if request.method == 'POST':
        if not request.user.is_authenticated():
           return redirect("%s?next=%s" % (reverse('account:login'),
                                                        request.path))
        if not apply_control_form(request):
            return HttpResponseBadRequest("Bad request")
    return None

def list_query(request, query):
    searchbox = agora.views.searchform(request)
    pg = Paginator(query, 10)
    page = request.GET.get('page')
    if page:
        q = request.GET.copy()
        q.pop('page')
        qstr = q.urlencode()
    else:
        page = 1
        qstr = request.GET.urlencode()
    if qstr:
        qstr = '&' + qstr

    try:
        pr = pg.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        pr = pg.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        pr = pg.page(pg.num_pages)

    ts = [ { 'p': p,
             'control_form': get_control_form(p, request.user),
             'breadcrumbs': get_breadcrumbs(p.place) } for p in pr ]
    params = request.GET.urlencode()
    control_callback = request.path_info
    if params:
        control_callback = "%s?%s" % (control_callback, params)
    return render(request, 'listing/standard.html', locals())

def standard(request):
    error = common(request)
    if error:
        return error

    t = { 'uc': 'creator',
          'um': 'moderators',
          'us': 'sponsors',
          'ug': 'followers',
          'uu': 'undecided',
          'uf': 'infavor',
          'ua': 'against',
          'ps': 'referencedBy__proposal',
          'pS': 'related',
          'pr': 'referencedBy__relation',
          'pR': 'link__relation',
          'pb': 'referencedBy__reverse_link__isnull', # isnull takes int only
          'pB': 'link__reverse_link__isnull', # isnull takes int only
          'pi': 'place' }
    try:
        qlist = [ Q((t[x], int(request.GET.get(x)))) if x in t else Q() for x in request.GET ]
        if qlist:
            q = Proposal.objects.filter(reduce(operator.and_, qlist))
        else:
            q = Proposal.objects.all()
    except ValueError:
        return HttpResponseBadRequest("Bad request")

    return list_query(request, q)

def list_place(request, place):
    error = common(request)
    if error:
        return error

    q = Proposal.objects.filter(place=place)
    return list_query(request, q)

def list_tag(request, tag):
    error = common(request)
    if error:
        return error

    q = Proposal.objects.filter(tags=tag)
    return list_query(request, q)

def list_search(request, proposals):
    error = common(request)
    if error:
        return error

    return list_query(request, proposals)
